; -*- mode: mynasm;-*-
;=============================================================================

; :file:   rabbit_lists.asm		working with lists

;=============================================================================
;   Rabbit-vm   An approach to the Joy Programming Language.
;   GNU GENERAL PUBLIC LICENSE, Version 3.0, http://www.gnu.org/licenses/
;   https://bitbucket.org/sts-q/rabbit-vm
;   Copyright (C) 2018 Heiko Kuhrt
;   Heiko.Kuhrt ( at ) yahoo.de
;=============================================================================
; :section:   macros
;-----------------------------------------------------------------------------
%macro llerr 1
%if GC_CHECKS == on
	strprint %1
	jmp f.llerr
%else
	jmp exit_1
%endif
	%endmacro

%macro check_list_element 3 ; ( value link err-dest -- )   may use:  a b d
%if GC_CHECKS == on
	mov d, %1				; d := value
	mov b, %2				; b := link
	push d
	push %3					; push err-dest
	call f.check_list_element
	rdrop					; err-dest
	pop d
%endif
	%endmacro

%if GC_CHECKS == on
f.llerr:
	strprint ": List.link failed."
	lf
	jmp exit_1


f.check_list_element:	
    .link:
	_ {cmp b, zero},	je  .value
	_ {cmp b, list_mem}, 	jl  .gc_err_list_link_small
	_ {cmp b, list_free},	jge .gc_err_list_link_big
	_ {test b, 15},		jnz .gc_err_list_link_unaligned

    .value:	
	wall? d, .gc_err_wall_value
	lst? d, .vlist
	sym? d, .vsym
	int? d, .vint
	jmp .gc_err_list_value_strange_type

    .vlist:
    	unlst d
	_ {cmp d, zero},	jne  .then
	ret
      .then:
	_ {cmp d, list_mem}, 	jl  .gc_err_list_value_small
	_ {cmp d, list_free},	jge .gc_err_list_value_big
	_ {test d, 15},		jnz .gc_err_list_value_unaligned	
	ret

    .vsym:
	unsym d
	_ {cmp d, first_atom_address}, 	jl  .gc_err_symbol_small
	_ {cmp d, list_free}, 	jge  .gc_err_symbol_big	
	_ {test d, 7},		jnz  .gc_err_symbol_not_aligned
	ret

    .vint:   ; what can we do here?  int is int isn't it
    	ret

    .gc_err_wall_value:
	_ lf, strprint "gc_err: list value is WALL", 				lf, rdrop, ret

    .gc_err_list_value_strange_type:
	_ lf, strprint "gc_err: list value is of strange type", 		lf, rdrop, ret

    .gc_err_list_value_small:
	_ lf, strprint "gc_err: list value points before list-space", 		lf, rdrop, ret
    .gc_err_list_value_big:
	_ lf, strprint "gc_err: list value points behind list-space", 		lf, rdrop, ret
    .gc_err_list_value_unaligned:
	_ lf, strprint "gc_err: list value not aligned", 			lf, rdrop, ret

    .gc_err_symbol_small:
	_ lf, strprint "gc_err: symbol points before first atom address", 	lf, rdrop, ret
    .gc_err_symbol_big:
	_ lf, strprint "gc_err: symbol points behind list space",	 	lf, rdrop, ret
    .gc_err_symbol_not_aligned:
	_ lf, strprint "gc_err: symbol is not aligned.", 			lf, rdrop, ret

    .gc_err_list_link_small:
	_ lf, strprint "gc_err: list link points before list space",		lf, rdrop, ret
    .gc_err_list_link_big:
	_ lf, strprint "gc_err: list link points behind list space",		lf, rdrop, ret
    .gc_err_list_link_unaligned:
	_ lf, strprint "gc_err: list link not aligned", 			lf, rdrop, ret

%endif	; GC_CHECKS == on



%macro List.next 0-3 ; ( -- elt-addr // using a b )
    %%again:
	mov a, [list_free]		; return address of first free element
	zero? a, %%collect
	jmp %%ok
    %%collect:
    	%rep %0
	    lst %1
	    push %1
	    %rotate 1
	    %endrep
	call List.gc
    	%rep %0
	    %rotate -1
	    pop %1
	    unlst %1
	    %endrep
	jmp %%again
    %%ok:
	mov b, [a + ws]
	mov [list_free], b		; store it as next free element

	
%if GC_CHECKS == on
	_ {mov b, [a]}, {cmp b, [border_1]}, jne error_nlb_not_wall
%endif
	%endmacro


; reg-for-new-link returns  new l
; reg-with-value is lst|int|sym, gets pushed to return stack always
; reg-with-link is l, gets pushed to return stack as lst
; append more l as parameter 5-7, they get pushed as lst
%macro List.link 4-7  ; ( reg-for-new-link  reg-with-value  reg-with-link err-dest   max-3-times-lst-push )
    %%again:
	mov a, [list_free]
	notzero? a, %%ok

    %%collect:
	push %2
	%ifn %3 == zero
		_ lst %3, push %3		
	%endif
    
	%rotate 4
    	%rep %0 - 4
	    _ lst %1, push %1
	    %rotate 1
	    %endrep
	    
	call List.gc
	
    	%rep %0 - 4
	    %rotate -1
	    _ pop %1, unlst %1
	    %endrep

	%rotate -4

	%ifn %3 == zero
		_ pop %3, unlst %3
	%endif
	pop %2
	jmp %%again

    %%ok:
    	mov b, [a + ws]
	mov [list_free], b
	
%if GC_CHECKS == on
	_ {mov b, [a]},  {cmp b, [border_1]},  jne error_nlb_not_wall_from_link
%endif
	
	mov [a],      %2
	mov [a + ws], %3
	mov %1, a

	check_list_element [%1], [%1 + ws], %4
	
	%endmacro

	
;=============================================================================
; :section:   init
section .text
;-----------------------------------------------------------------------------
List.init:

	mov di, list_mem		; address of last list element
	mov a,  zero			; last list element points to nil
	mov c,  maximal_list_elements
	mov d, WALL

    .loop:
    	mov [di], d
    	mov [di + ws], a		; write pointer to next element
	mov a, di			; destination of next pointer to write
    	add di, 16			; inc pointer
	loop .loop

	sub di, 16			; correct last add of loop
	mov [list_free], di		; write initial start of list of free elements
	ret				; return to calling subroutine
	
;=============================================================================
; :section:   operations on lists
;-----------------------------------------------------------------------------
%macro i.nil 0
	up sos
	mov sos, tos
	mov tos, nil
	%endmacro
	
%macro cons 0
	notlst? tos, error_list_expected
 	unlst tos
	List.link tos,  sos, tos,  err_ll_failed_cons
	lst tos
	down sos
	%endmacro

%macro conc 0 ; ( l x -- l  // l ++ (x) mutating l, RETURNING NEW LAST elt of l )
	notlst? sos, error_list_expected
	nil? sos, err_list_is_nil
	mov si, sos			; l
	down sos
	unlst si
	mov d, [si + ws]
	jmp %%while

    %%loop:
    	mov si, d
	mov d, [si + ws]
    %%while: notzero? d, %%loop; elt pointing to nil -> last found
	
    %%done:
    	List.next si			; better with List.next, not List.link
	mov [si + ws], a		; let last point to new last
	mov [a],       tos		; let new last contain tos
	mov [a + ws],  zero		; let new last point to nil
	mov tos, a			; return new last elt
	lst tos
	%endmacro

	
%macro uncons 0
	notlst? tos, error_list_expected
	nil?    tos, error_uncons_nil
	unlst   tos
	up sos
	mov sos, [tos]
	mov tos, [tos+ws]
	lst tos
	%endmacro

%macro length 0
	notlst? tos, error_list_expected
	mov a, tos
	unlst a
	clr c
	
	jmp %%while
    %%loop:
    	mov a, [a + ws]
	inc c
    %%while: notzero? a, %%loop
	
	mov tos, c
	int tos
	%endmacro
	
i.length:
	length
	ret
	
%macro m4.equal 3 ; ( l l -- b ) 	; test if all elements of two lists are equal
					; and both lists of same length, not recursive 
	mov a, %1			; l1
	mov b, %2			; l2
	jmp %%while
	
    %%loop:
    	mov c, [a]			; get
	mov a, [a + ws]			; step
	mov d, [b]
	mov b, [b + ws]
	_ {cmp c,d}, jnz %%no		; compare
    %%while: zero?    a, %%second_nil_too	; first list ended, second?
     	     notzero? b, %%loop
	jmp %%no
	
    %%second_nil_too:			; first list is nil, second?
	zero? b, %%yes
	jmp %%no

    %%yes:
    	mov %3, one			; return one or 1, doesn't hurt anyway as long as cmp to zero
	jmp %%all_done
    %%no:
    	xor %3,%3			; return false
    %%all_done:
	%endmacro

%macro reverse 0 ; ( l -- l  // reverse to new list using: tos si di a d )
	notlst? tos, error_list_expected
	mov si, tos			; si := l
	unlst si
	mov di, zero			; di := newl
	clr tos
	
	jmp %%while
    %%loop:
    	mov d,        [si]		; d := item
	mov si,       [si + ws]		; si := rest( l )
	List.link di,  d,di,  err_ll_failed_reverse,  si
    %%while: notzero? si, %%loop	; repeat until old list == nil

	mov tos, di			; replace tos with new list
	lst tos
	%endmacro

%macro rev 0 ; ( l -- l  // reverse in place using: a b tos )
	notlst? tos, error_list_expected
	_ {cmp tos, nil}, jz %%all_done
	unlst tos
	
	mov a, [tos + ws]		; elt points to next elt: a
	mov [tos + ws], zero		; write nil into now last elt

	jmp %%while
    %%loop:
	mov b,   [a + ws]		; remember next elt
      	mov [a + ws], tos		; write prev elt
    	mov tos, a			; move tos
	mov a, b			; move a
    %%while: notzero? a, %%loop		; elt points to nil: done
	
    %%done:
	lst tos				; last elt of old list, did point to nil
    %%all_done:
	%endmacro

f.rev:
	push tos
	mov tos, a
	lst tos
	rev
	mov a,tos
	unlst a
	pop tos
	ret
	
%macro concat 0   ; ( l1 l2 -- newl )
	notlst? tos, error_list_expected
	notlst? sos, error_list_expected
	xchg tos, sos			; tos := reverse( l1 )   save l2 in sos while reversing
	reverse
	mov si, tos			; si := reversed l1
	mov di, sos			; di := l2
	unlst si
	unlst di
	down sos			
	clr tos

	jmp %%while
    %%loop:
	mov d,      [si]
	mov si,     [si + ws]
	List.link  di,  d, di, err_ll_failed_concat,  si
    %%while:  notzero? si, %%loop

	mov tos, di
	lst tos
	%endmacro
	
%macro append 0 ; (l1 l2 -- l3  // append l1 to l2, mutating tail of l1 )
	notlst? tos, error_list_expected
	notlst? sos, error_list_expected
	_ {cmp tos, nil}, jz %%return_sos
	_ {cmp sos, nil}, jz %%return_tos
	mov si, sos				; si := l1
	mov di, tos				; di := l2
	unlst si
	unlst di
	
	mov d, [si + ws]			; d := rest( l1 )
    %%while: zero? d, %%done
    	mov si, d				; si := rest( l1 )
	mov d, [si + ws]			; d := rest( rest ( l1 ))
	jmp %%while
    %%done:

    %%return_concatenation:
    	mov [si + ws], di			; link-of-last-l1-elt := l( l2 )
	mov tos, sos				; tos := lst( l1 )
	down sos
	jmp %%all_done

    %%return_tos:
    	down sos
    	jmp %%all_done
	
    %%return_sos:
	mov tos, sos
	down sos
	
    %%all_done:
	%endmacro

%macro copy 0 ; ( l -- l )
	notlst? tos, error_list_expected
	mov si, tos
	unlst si				; si := l
	mov v,  [si]				; v  := first value of l
	mov si, [si + ws]			
	List.link di,   v, zero,   err_ll_failed_copy,   si
	mov tos, di				; start of copy ready for return
	lst tos

	jmp %%while
    %%loop:
    	mov v,  [si]				; v  := next value of l
	mov si, [si + ws]
    	List.link k,   v, zero,   err_ll_failed_copy,   si	; di is save over tos
	mov [di + ws], k			; prev elt points to this one
	mov di, k				; now this is next prev
    %%while notzero? si, %%loop

	%endmacro

;=============================================================================
; :section:   string  <-->  list
;-----------------------------------------------------------------------------
f.string0_to_list: ; ( startaddr -- l // make list of 0-terminated string )   callsave
	mov si, a			; si := startaddr
	clr d				; d  := char
	mov dl, byte [si]
	inc si
	clr di				; di := new list
	jmp .while

    .loop:
	int d
	List.link di,  d, di,  .err_ll_failed
	clr d				; d := next-chr
	mov dl, byte [si]
	inc si
    .while: notzero? d, .loop
    	
	mov a, di
	call f.rev
	ret

    .err_ll_failed: llerr  "string0_to_list"

; --------------------------------------------------
f.string_to_list: ; ( startaddr endaddr+1 -- l )		callsave
	mov si, a			; si := startaddr
	mov di, b			; di := endaddr
	mov k, zero			; k  := new list
	clr d				; d  := char
	jmp .while
	
    .loop:
	dec di
	clr d	
	mov dl, byte [di]
	int d
	List.link k,  	d, k,  .err_ll_failed
    .while: _ {cmp si, di}, jl .loop

	mov a, k			; return k := new list
	ret
	
    .err_ll_failed: llerr "string_to_list"
	
; --------------------------------------------------
f.list_to_string0: ; (l -- i:len // write chars of l into stribu)
	mov si, a			; si := l
	call Stribu.clear
	
    	jmp .while
    .loop:
	mov d,  [si]			; a := cc
	mov si, [si + ws]
	notint? d, .err_no_printable_char
	uti d
	_ {cmp d,   0}, jl .err_no_printable_char
	_ {cmp d, 255}, jg .err_integer?
	mov a, d
	call Stribu.append_char
	_ {cmp d, 10}, jne .while
		mov a, 13
		call Stribu.append_char
    .while: notzero? si, .loop

    	call Stribu.close		; return current number of char in stribu
	ret
    .err_no_printable_char:  ; ( XXX strange chr in d XXX )
	mov [buffer], zero
	error `List.>string0: some list item is probably not a printable character.\nchrcode: `, buffer, d
    .err_integer?:  ; ( XXX strange chr in d XXX )
	mov [buffer], zero
	error `List.>string0: some list item is probably an integer.\nchrcode: `, buffer, d

	
f.list_to_string0_appending: ; ( l -- // append char-list l to Stribu, terminate with zero )
	mov si, a			; si := l

	jmp .while
    .loop:
    	mov a,  [si]
	mov si, [si + ws]
	uti a
	call Stribu.append_char
    .while: _ {cmp si, zero}, jne .loop

    	call Stribu.close
	ret

	
; --------------------------------------------------
f.list_print: ; ( l -- // print list ) a
	callfun   f.list_to_string0,  	a,b,d,   si,di,k,v,c
	callfun   Stribu.print0,	a,b,d,   si,di,k,v,c
	ret


;=============================================================================
; :section:   Sourcebuffer
;-----------------------------------------------------------------------------
Sourcebuffer.print: ; ( -- // print current Sourcebuffer )
	print0 source_buffer
	ret

Sourcebuffer.current: ; ( -- i  // current first free position )
	mov a, [source_buffer_current]
	ret

Sourcebuffer.append: ; ( l --  // append char from list l to current contents of sourcebuffer )
        notlst? tos, .err_no_list
	shiftdown si				; si := l
	unlst si
	mov   di, source_buffer
	

    .while: zero? si, .done
	mov a, [si]
	uti a
	mov si, [si + ws]
	mov [di], al
	inc di
    	jmp .while
	
    .done:
	mov [di], zero
	inc di
	sub di, source_buffer
	mov [source_buffer_current], di
	ret
    .err_no_list: error "Sourcebuffer.append: list on stack expected."

Sourcebuffer.content:
	call Sourcebuffer.current
	mov si, a
	sub si, 1				; first-free -> last char, without trailing zero
	add si, source_buffer			; si  := last chr in sourcebuffer
	mov k,  source_buffer			; k  := sourcebuffer-start  (for si downto k)
	mov di, zero				; di := new list of chars

	jmp .while
    .loop:
	clr d
	mov dl, byte [si]
	int d
	dec si
	List.link di,  d, di,  .err_ll_failed
    .while: _ {cmp si, k}, jge .loop

	mov a, di				; return new list
	ret

    .err_ll_failed: llerr "Sourcebuffer.content"

    
;=============================================================================

