; -*- mode: mynasm;-*-
;=============================================================================

; :file:   rabbit.asm   		rabbits 'main' assembler file
; Jim Gray - Computing is fractal; wherever you look, there is infinite complexity.
;=============================================================================
;   Rabbit-vm   An approach to the Joy Programming Language.
;   GNU GENERAL PUBLIC LICENSE, Version 3.0, http://www.gnu.org/licenses/
;   https://bitbucket.org/sts-q/rabbit-vm
;   Copyright (C) 2018 Heiko Kuhrt

;   Heiko.Kuhrt ( at ) yahoo.de
;=============================================================================

%defstr osname		%!RABBIT_COMPILES_ON
%if osname == 'linux'
	%define on_linux  1
%elif osname == 'dragon'
	%define on_dragon 1
%else
	%error "Unknown OS."
%endif

;=============================================================================
; :section:   include			include or inline all other assembler files
;-----------------------------------------------------------------------------
global lf_x86_64			; entry point for ld -elf_x86_64
global main

%include "rabbit_declarations.asm"
%include "rabbit_prelib.asm"
%include "rabbit_syscall.asm"
%include "rabbit_lib.asm"
%include "rabbit_types.asm"
%include "rabbit_stack.asm"
%include "rabbit_lists.asm"
%include "rabbit_gc.asm"
%include "rabbit_symtab.asm"
%include "rabbit_parse.asm"
%include "rabbit_atoms.asm"
%include "rabbit_atomsadd.asm"

section .text
;=============================================================================
; :section:   command_line		parse and exec commandline
;-----------------------------------------------------------------------------
commandline:
	mov a, source_buffer
	call collect_commandline
	callfun Parse.rabbit.pe, source_buffer, mess_commandline
	_ {cmp a, 0}, jl .failed
	
    .success:
   	unlst a
	call f.execute
	_ call Stacks.ds_load, {cmp a, 0}, jnz .print
	_ call Stacks.ss_load, {cmp a, 0}, jnz .print
	ret
    .print:
;    	stacks
	ret
	
    .failed:
    	strprint "Parsing commandline failed."
	lf
	ret

	
;=============================================================================
; :section:   restart on error		exec definition VM.error if defined, else exit_1
;-----------------------------------------------------------------------------
Exec.vm_error:
	call Stacks.init			; ds ss rs
	mov a, vm_errorfun_name			; VM.error
	call f.string0_to_list
	mov k, a
	callsave Symtab.index, a, a		; get definition of VM.error
	_ {cmp a, zero}, je .exit_1
	
    .call_errfun:		
    	mov a, k
	lst a
	call Symtab.fn
	mov tos, a
	call f.execute
	
    .exit_1:			
    	jmp exit_1

	
;=============================================================================
; :section:   main			init, exec commandline and exit
;-----------------------------------------------------------------------------
%ifdef on_linux
%warning "Compiling for Linux."
;-----------------------------------------------------------------------------
main:
lf_x86_64:
	mov a, [rsp]				; commandline args: count and value
	mov [argc], a
	mov [argv], rsp
	
	clr zero				; reg zero: just and always zero
	call Syscall.stdin_init			; stdin -> nonblocking,   set rabbit_start_time
	call Stacks.init			; writes rs_base
	call List.init
	call Symtab.init
	call Symtab.addatoms
	call Stacks.clear			; ds ss

	call commandline

quit:
 	call Stacks.check_borders

	jmp exit

;=============================================================================
%elifdef on_dragon
%warning "Compiling for DragonFly."
;=============================================================================
main:
lf_x86_64:
quit:
	mov rax, 65
	mov [buffer], rax
	mov rax, 4
	mov rdi, 1
	mov rsi, buffer
	mov rdx, 1
	syscall
	mov rax, 66
	mov [buffer], rax
	mov rax, 4
	mov rdi, 1
	mov rsi, buffer
	mov rdx, 1
	syscall
	mov rax, 67
	mov [buffer], rax
	mov rax, 4
	mov rdi, 1
	mov rsi, buffer
	mov rdx, 1
	syscall
	mov rax, 10
	mov [buffer], rax
	mov rax, 4
	mov rdi, 1
	mov rsi, buffer
	mov rdx, 1
	syscall
	
	mov rdx, 1
	syscall

;=============================================================================
%else
quit:
%error "something went wrong."
%endif
;=============================================================================

	call a.swap
	jmp list_mem
	call a.dup
	jmp list_mem
	call a.concat
	jmp list_mem
	call a.add
	jmp list_mem
	cmp tos, zero
	cmp tos, 0
	xor a,a
	mov [tos], a
	mov [tos], zero

	mov a, tos
	and al, TYPE_MASK
	cmp al, 2

	test tos, 2			; jump in caller flipped!!
	test al, 2
	test bp, 2
	test r15b, 2

	clr al
	shld tos, a, 2
	cmp a, (1 << 30)

	cmp dsp, border_1
	cmp ebp, border_1
	cmp dsp, tos
	cmp ebp, edx
	cmp ebp, r15d
	mov a, d
	mov b, prog
	mov ebx, r15d
	mov prog, [prog + ws]
	mov r15d, [r15d + ws]
        zero? prog, quit
        test r14d,r14d
        jz quit
        cmp r14, 0
        cmp r14d, 0
        cmp r14, r15
        cmp r14d, r15d


; 63/57266
	mov al, dl			; notsym?
	and al, TYPE_MASK
	cmp al, 3
	jnz quitt			; not a symbol  ->  just cons: int|lst
	sub dl, al

; 65/57266
        test dl, 3
        jnp quitt
        jz quitt     
        and dl, -4
        
; 65/57266
        mov eax,edx
        mov rax,rdx
        test al, 3
        jnp quitt
        jz quitt     
        and al, -4
quitt:
        mov d,k
        unlst d
        call f.execute
        mov a,k
        unlst a
        call f.execute

; mov eax
        mov r14, rax
        mov r14d, eax
; bt
        bt ax, 0
        bt eax, 0
        bt rax, 0
        bt si,0
        bt r15, 0
        bt r15d,0

; eins
        mov al, dl
        and al, 3
        cmp al, 2
        jz quitt
        and dl, -4
        and d, -4
        and edx, -4
; eins-b
        mov dl, al
        and dl, 3
        cmp dl, 2
        jz quitt
        and al, -4
        and a, -4
        and eax, -4
;zwei
        test al, 3
        jp quitt
        jz quitt

;drei
        test al, 2
        bt eax, 0
        jna quitt

;inc
        add sstos, 1
        inc sstos
        add tos, 1
        inc tos
        add a,1
        inc a

;unconsLst
        unlst tos
        mov sos, [tos]
        mov tos, [tos + ws]
        lst tos
;unconsLst
        mov sos, [tos - LST]
        mov tos, [tos + ws - LST]
        lst tos

;=============================================================================
        add a,b
        add a,c
        add b,a
        add b,c
        add c,a
        add c,b
        add tos,sos
        add tos,1
        add a,1
        add a, 1
        sub a,b
        sub a,c
        sub b,a
        sub b,c
        sub c,a
        sub c,b
        cmp a,b
        cmp a,c
        cmp b,c
    .here:
        add a,4
        cmp sos,a
        cmp a,sos
        jl .here
        jg .here
        jmp .here 
        mov a,tos
        mov a,sos
        mov tos,a
        mov sos,a
        mov b,tos
        mov b,sos
        mov tos,b
        mov sos,b
        mov c,tos
        mov c,sos
        mov tos,c
        mov sos,c
        mov b, 1
        mov rbx,1
        mov ebx,1
        mov bx,1
        mov a, 1
        mov rax,1
        mov eax,1
        mov ax,1

        shl tos,1
        shl sos,1
        sar tos,1
        sar sos,1
        shl tos,2
        shl sos,2
        sar tos,2
        sar sos,2


        
        
;=============================================================================
