; -*- mode: mynasm;-*-
;=============================================================================

; :file:   rabbit_parse.asm		parse rabbit source to AST

;=============================================================================
;   Rabbit-vm   An approach to the Joy Programming Language.
;   GNU GENERAL PUBLIC LICENSE, Version 3.0, http://www.gnu.org/licenses/
;   https://bitbucket.org/sts-q/rabbit-vm
;   Copyright (C) 2018 Heiko Kuhrt
;   Heiko.Kuhrt ( at ) yahoo.de
;=============================================================================
; :section:   scan
section .text
;-----------------------------------------------------------------------------
%macro getcc 0
	mov cl, byte [si]
	%endmacro
%macro cmpcc 1
	cmp cl, %1
	%endmacro
%macro nextcc 0
	inc si
	getcc
	jmp .while
	%endmacro
%macro inc_line 0
	mov a, [CurrentLine]
	inc a
	mov [CurrentLine], a
	%endmacro
;-----------------------------------------------------------------------------
%macro word? 2 ; ( then-dest else-dest )  ; cc pointing to word-char?
	mov b, word_characters
	mov al, byte [b]
    %%while:
    	cmp al, cl
	jz %1				; yes: jmp %1
    	cmp al, 0
	jz %2				; no, not found: jmp %2
	inc b
	mov al, byte [b]
	jmp %%while
	%endmacro
	
%macro string? 2 ; ( iftrue-goto  iffalse-goto ) 
	_ cmpcc '"', je %1		; string1
	_ cmpcc 39,  je %1		; string2  '
	_ cmpcc '{', je %1		; string3 == docstring
	jmp %2
	%endmacro
	
; -----------------------------------------------------------------------------
%macro do_comment 0
    %%while:
    	cmp c, 10
	jz %%done
	cmp c, 0
	jz %%done
	inc si
	mov cl, byte [si]
	jmp %%while
    %%done:
	%endmacro

%macro do_accent 0
	mov a, si			; a := word-start
	inc si
	getcc

    %%while: _ {cmp c, '^'}, jnz %%done	; find word-end
    	inc si
	getcc
	jmp %%while
    %%done:
    
	mov b, si			; b := word-end + 1
	callsave f.string_to_list, a
	lst a
	callsave Symtab.fn, a
	sym a
	up sos				; cons word
	mov sos, a
	cons
	%endmacro			; si pointing to first char behind ^^^

%macro do_special 0
	mov a, si
	mov b, si
	inc b
	callsave f.string_to_list, a,b
	lst a
	callsave Symtab.fn, a
	sym a
	up sos
	mov sos, a
	cons
	%endmacro

%macro do_word 0
	mov k, si			; k := index of first char
    %%while:				; find first non-word-char-index in si
    	word? %%step, %%then
    %%step:
    	inc si
	getcc
	jmp %%while
	
    %%then:	
	mov v, si			; v := first non-word-char

	mov si, k			; is it an integer?
	getcc
	cmpcc '+'
	jz %%skip_sign
	cmpcc '-'
	jz %%skip_sign
	jmp %%while_2
    %%skip_sign:
    	inc si
	getcc
	cmp si, v			; just a sign, no more chars  ->  scan_word
	jz %%scan_word
    %%while_2:				; all chars (0 <= c <= 9)  ->  scan_int
    	cmp si, v
	jz %%scan_int
	cmpcc '0'
	js %%scan_word
	cmpcc '9'
	jg %%scan_word
	inc si
	getcc
	jmp %%while_2

    %%scan_word:			; cons word to ts  k := first-cc  v := first not-word-cc
    	callsave f.string_to_list, k, v	
	lst a
	callsave Symtab.fn, a		; find or create symbol
	sym a
	up sos				; cons( sym, ts )
	mov sos, a
	cons
	jmp %%done
    
    %%scan_int:				; cons int to ts
	callsave f.iscan, k,v
	liti a
	swap
	cons
	
    %%done:
    	mov si, v			; restore src-index
	getcc
	%endmacro

; -----------------------------------------------------------------------------
; :section:   do_string

%macro do_string 0 ; cc at opening '"{,   cc behind closing '"} when done
	_ {mov a, '"'}, cmpcc '"', {cmovz d, a}
	_ {mov a,  39}, cmpcc  39, {cmovz d, a}
	_ {mov a, '}'}, cmpcc '{', {cmovz d, a}
	inc si
	mov k, si			; k := pos( first-char-of-string )
	mov v, d			; v := type of collected string
	getcc
	
    %%while:
    	_ cmpcc 0,  je %%error_missing_terminator
	_ cmpcc dl, je %%string_end_found
	_ cmpcc 10, jne %%nl_done
    	inc_line
    %%nl_done:
	inc si
	getcc
	jmp %%while

    %%error_missing_terminator:
    	mov a, mess_string_terminator_missing
	mov b, d
	jmp .fail

    %%string_end_found:
	callsave f.string_to_list, k, si  ; si := pos( first-not-string-char )
	
    %%docstring:
	_ {cmp v, '}'}, jnz %%docstring_done
	mov k, a			; d := char-list
	call Symbol.docstring
	mov v, a			; v := Symbol.docstring
	List.link  di,  v,k,  err_ll_failed_parse
	mov a, di
    %%docstring_done:
    
	lst a
	up sos
	mov sos, a
	cons
	inc si				; skip closing '"}
	getcc
	%endmacro
; -----------------------------------------------------------------------------
; :section:   scan
Parse.scan: ; ( pSrc -- ts )		; ts are collected in tos, returned in a
	i.nil				; tos := nil     push new list of ts to stack
	mov si, a			; pointer to current position in source
	xor c,c
	getcc				; current char

    .while: _ cmpcc 0, jz .done	; source must be 0-terminated
    	.whitespace:
		_ cmpcc ' ', jz .next
		_ cmpcc 9,   jz .next

	.newline_0A:
		cmpcc 10
		jnz .newline_0D
		mov cl, byte [si + 1]
		_ cmpcc 13, jnz .no_0D
			inc si
		.no_0D:
		   	inc_line
		   	nextcc
	.newline_0D:
		cmpcc 13
		jnz .comment
		mov cl, byte [si + 1]
		_ cmpcc 10, jnz .no_0A
			inc si
		.no_0A:
		   	inc_line
		   	nextcc
	.comment:
		cmp c, ';'
		jnz .accent
			do_comment
			jmp .while
	.accent:
		_ cmpcc '^', jnz .special
		    .yes_accent:
		    	do_accent
			jmp .while
	.special:
		_ cmpcc '(', jz .yes_special
		_ cmpcc ')', jz .yes_special
		_ cmpcc '|', jz .yes_special
		_ cmpcc '#', jz .yes_special
		_ cmpcc '`', jz .yes_special
		_ cmpcc ':', jz .yes_special
		_ cmpcc ',', jz .yes_special
		jmp .word
		    .yes_special:
			do_special
			nextcc
	.word:
		word? .yes_word, .string
		    .yes_word:
			do_word
			jmp .while
			
	.string:
		string? .yes_string, .strange
		    .yes_string:
			do_string
			jmp .while
		
	.strange:
		push c
		strprint ">>>"
		pop c
		iprint c
		strprint "<<<"
		mov a, mess_strange_char_found
		jmp .fail

    .next:
	nextcc
	
    .done:
    	rev				; reverse ts
    	mov a, tos			; return ts
	mov tos, sos
	down sos			; drop ts from stack
	ret
	
    .fail:
	neg a
	ret
;=============================================================================
; :section:   built AST
;-----------------------------------------------------------------------------
Parse.parse: ; (l:ts -- l:nts)	make AST from flatt list of tokens
	unlst a
	mov si, a			; si := ts
	mov di, zero			; di := nts
    _ lst si, lst di, push si, push di
	call Symbol.open_paren
	mov c, a			; c  := symbol'('
	call Symbol.close_paren
    _ pop di, pop si, unlst si, unlst di
	mov d, a			; d  := symbol')'
	mov [Parse.rs_base], rsp	; where to got in case of error
	push '#'

    .while: _ {cmp si, zero}, je .done
        mov v,  [si]
    	mov si, [si + ws]
	_ {cmp v, c}, je .open_paren
	_ {cmp v, d}, je .close_paren
    .just_a_t:
	push d				;  )
	List.link  di, v, di,  err_ll_failed_parse,  si
	pop d
    	jmp .while

    .open_paren:
    	lst di
    	push di
	push '('
	clr di
    	jmp .while
	
    .close_paren:
    	mov a, di			; rev nesting list
	call f.rev
	mov v, a
	lst v				; v  := lst( list/nesting )
	pop a
	_ {cmp a, '('}, jne .open_paren_missing
	pop di				
	unlst di			; di := l( list/parent )
	push d				; )
	List.link  di,  v,di,  err_ll_failed_parse, si
	pop d
    	jmp .while

    .done:
	pop a
	_ {cmp a, '#'}, jne .parens_dont_match
	mov a, di
	call f.rev				; return  a := l(ast)
	ret

    .open_paren_missing:
    	mov a, mess_open_paren_missing
	neg a
	ret
    .parens_dont_match:
    	mov a, mess_closing_paren_missing
	neg a
	mov rsp, [Parse.rs_base]
	ret
;=============================================================================
; :section:   parse rabbit
;-----------------------------------------------------------------------------
Parse.print_error: ; ( mess_fail -- )
	print0 a
	lf
	strprint "file: "
	print0 [Parse.source_file_name]
	lf
	strprint "line: "
	iprint [CurrentLine]
	lf
	ret
	
Parse.rabbit:  ; ( *buffer *str0:file-name -- )
	clr k, v, si, di
	clr c, d
	mov [Parse.source_file_name], b
	push a
	callsave f.string0_to_list, b
	lst a
	mov [CurrentSource], a			; CurrentSource := lst(file_name)
	pop a					; a := source-buffer
	mov b, 1
	mov [CurrentLine], b			; Parse.current_line := 1
	call Parse.scan				; a := lst(ts)
	cmp a, 0
	js .scan_failed
	call Parse.parse			; a := l(ast)
	ret
    .scan_failed:
    	ret

Parse.rabbit.pe: ; ( buffer file-name -- l )     print error
	call Parse.rabbit
	cmp a, 0
	js .failed
	ret

    .failed:
    	neg a
    	call Parse.print_error
	mov a, -1
	ret
	
err_ll_failed_parse: error "Parse.rabbit: List.link failed."
;=============================================================================
