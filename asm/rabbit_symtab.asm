; -*- mode: mynasm;-*-
;=============================================================================

; :file:   rabbit_symtab.asm		symbol table: table of all seen symbols

;=============================================================================
;   Rabbit-vm   An approach to the Joy Programming Language.
;   GNU GENERAL PUBLIC LICENSE, Version 3.0, http://www.gnu.org/licenses/
;   https://bitbucket.org/sts-q/rabbit-vm
;   Copyright (C) 2018 Heiko Kuhrt
;   Heiko.Kuhrt ( at ) yahoo.de
;=============================================================================
; :section:   symtab
;-----------------------------------------------------------------------------
section .text

error_symtab_overflow: 		error "Symtab.create: symtab overflow."
error_define_expects_list: 	error "define: list on stack expected"
error_define_expects_symbol: 	error "define: symbol on stack expected"

Symtab.init:
	mov a, symtab			; first is first free entry
	mov [symtab_ff], a
	mov a, initial_source
	callsave f.string0_to_list, a, b
	lst a
	mov [CurrentSource], a		; CurrentSource := "iniSrc"

	mov [CurrentLine],    zero	; CurrentLine := 0
	ret

; --------------------------------------------------
Symtab.index: ; ( l:name -- pFunc|0 )
	mov k, a
	mov si, symtab
	mov di, [symtab_ff]
	jmp .while

    .loop:
    	mov a, [si + o_name]
	unlst a
	m4.equal a, k, a		; if name == Symtab[i].name goto found
	_ {cmp a, zero}, jnz .found
	add si, tFunc_size
    .while: _ {cmp si, di}, jl .loop	; while si < ff loop

    .not_found:
    	clr a				; return 0
	ret
	
    .found:
    	mov a, si			; return pFunc
	ret

; --------------------------------------------------
Symtab.pos: ; ( fn:fn -- pFunc|0 )
	mov k, a
	mov si, symtab
	mov di, [symtab_ff]
	jmp .while

    .loop:
	_ {cmp k, [si]}, je .found	; if fn == Symtab[i].fn goto found
	add si, tFunc_size
    .while: _ {cmp si, di}, jle .loop

    .not_found:
    	clr a
	ret
	
    .found:
    	mov a, si
	ret
	
; --------------------------------------------------
Symtab.create: ; ( lst:name lst:docstring fn:fn -- pFunc )  	a
	push b				; push docstring
	mov b, [symtab_ff]
	_ {cmp b, symtab_last}, jg error_symtab_overflow

	mov [b], d			; fn

	add b, ws
	mov [b], a			; name

	pop a
	add b, ws
	mov [b], a			; docstring

	mov a, [CurrentSource]		
	add b, ws
	mov [b], a			; pSrc

	mov a, [CurrentLine]
	add b, ws
	mov [b], a			; line

	add b, ws
	mov [symtab_ff], b

	mov a, b			; return pFunc
	sub a, tFunc_size

	ret

;=============================================================================
; :section:   interface
;-----------------------------------------------------------------------------
Symtab.fn: ; ( lst:name -- fn:fn  // return fn, create if not found )   callsave
	mov k, a			; k := lst:name
	unlst k
	mov si, symtab			; si := Symtab[0].*fn
	mov di, [symtab_ff]		; di := Symtab[first_free].*fn

    .loop:
    	mov a, [si + o_name]
	unlst a
	m4.equal a, k, a
	_ {cmp a, zero}, jnz .found
	add si, tFunc_size		; first cmp, than shift si
    .while: _ {cmp si, di}, jl .loop

    .not_found:				; not found -> create new symbol
	
	mov a, k			; 	name
	lst a
	mov b, nil			; 	docstring	:= {}
	mov d, nil			; 	fn    	:= ???
	call Symtab.create 		; 	returns func
	mov k, a			; k :=  Symtab[a].*fn		lst:name gets gc-saved here

	mov d, a.nodef				; build: ( ` name nodef )
 	sym d
	List.link si,      d,zero,  .err_ll_failed
	List.link si,  zero,si,     .err_ll_failed
	mov di, si				; where name-symbol should be
	mov d, a.wrd
	sym d
	List.link si,     d,si,	    .err_ll_failed,  di
	mov a, si				; this is the name-symbol
	sym a
	mov [di], a				; write name-symbol 

	mov [k], si				; Symtab[a].fn := fn    fn := ( `fn nodef )
	mov a, si				; return fn

	ret

    .found:					; si := Symtab[name].*fn
    	mov a, [si]				; return fn:	addr of first elt of quotation in list-space
						;		or atom-addr
	ret

    .err_ll_failed: error "Symtab.fn: List.link failed."
	
; --------------------------------------------------
Symtab.symbol: ; ( fn:fn -- lst:name|nil  // opposite of Symtab.index )
	mov k, a			; pFn:atom|quotation
	mov si, symtab
	mov di, [symtab_ff]
	jmp .while
	
    .loop:
	_ {cmp k, [si]}, je .found
    	add si, tFunc_size
    .while: _ {cmp si, di}, jl .loop
    
    .not_found:
    	mov a, nil			; return nil
	ret
    
    .found:
	mov a, [si + o_name]		; return Symtab[si].name
	ret

	
; --------------------------------------------------
Symtab.add: ; ( str0:name str0:docstring atom -- )   used just and only by 'addatom'
	mov k, a			; name
	mov v, b			; docstring	
	mov di, d			; atom

	callsave f.string0_to_list, k, b
	mov k, a
	lst k
	callsave f.string0_to_list, v, b
	mov v, a
	lst v

	mov a, k
	mov b, v
	mov d, di
	call Symtab.create

	ret

; --------------------------------------------------
Symtab.symbols: ; ( -- l:symbols  // return list of all symbols )  used by just and only 'Symbol.list'
	mov si, symtab
	mov di, [symtab_ff]
	mov v, zero			; v := new list of symbols
	jmp .while
	
    .loop:
	mov d, [si]			; b := symbol
	sym d
	List.link  v,  d,v,  .err_ll_failed
	add si, tFunc_size
    .while: _ {cmp si, di}, jl .loop
    	mov a, v
	ret

    .err_ll_failed: error "Symtab.symbols: List.link failed."
; --------------------------------------------------
Symtab.dump: ; ( -- )
	strprint `\r=================  symtab  =================           `
	lf
	mov si, symtab
	mov di, [symtab_ff]
	xor c,c
	
	jmp .while
    .loop:
	cprint '`'
		mov a, [si + o_name]
		unlst a
		call f.list_print
		cprint 9
		
	cprint '{'
 		mov a, [si + o_docstring]
		unlst a
		call f.list_print
		strprint `} \t`
		
    	mov a, [si]			; fn
	_ {cmp a, last_atom_address}, jle .print_atom
    .print_quotaion:
	lst a
		callsave f.lprint, a, b
    		cprint ' '
	jmp .print_src
	
    .print_atom:
	push a
    	cprint '#'
		pop a
    		callsave Symtab.symbol, a, b
		unlst a
		call f.list_print
	    	cprint 9

    .print_src:
	strprint "<"
		mov a, [si + o_pSrc]
		unlst a
		call f.list_print
		spc
		iprint [si + o_line]
	strprint ">"
	
    	lf
	add si, tFunc_size
	inc c
    .while: _ {cmp si, di}, jl .loop


    	_ strprint "----  ", iprint_ c, strprint "entries  ----", lf
	ret
	
; -----------------------------------------------------------------------------
Symtab.define: ; {(sym) lst:docstring lst:def -- // define new definition}
	; Symbol is already in Symtab, we don't need to create, just set docstring and fn
	down k				; k  := sym( symbol )
	mov v, sos			; v  := lst( docstring )
	mov di, tos			; di := lst( def )
	down tos
	down sos
	notsym? k,  error_define_expects_symbol	
	notlst? v,  error_define_expects_list	
	notlst? di, error_define_expects_list
	unsym k				; k  := symbol
	_ unlst v, {mov v, [v + ws]}, lst v	; v := lst( drop symbol docstring from lst:docstring )
	unlst di			; di := l( def )

	callsave Symtab.pos, k, b	; a := Symtab.pos(fn)		no List.next
	mov k, a
	mov [k + o_docstring], v	; Symtab.pos(fn).docstring := lst:docstring

	_ {cmp di, zero}, jnz .A	; if def != nil goto (.A)

	; `name {} () define  ->  `name {} (nop) define
	; because we can't define a symbol to nil, because nil is not a list-space-element
	mov k, [k]			; k := (nodef), first elt of old def
	mov a, a.nop
	sym a
	mov [k], a			; value := nop
	mov [k + ws], zero		; link  := 0
	ret

    .A:
; (A)	; don't change start position of definition stored in this symtab.pos
	; --> rewrite first item of old definition

	;		link:  rest of new def
	;		value: first of new def
	mov k, [k]			; k := (nodef), that is pointer to first elt of old def
	mov a, [di]			; link to second elt of new def
	mov b, [di + ws]		; first elt of new def
	mov [k],      a			; link to second elt of new def
	mov [k + ws], b			; first elt of new def

; (B)	; write new definition into symtab
	; 	lookup will fail from now on
	;	changing the definition will have no effect for existing uses.
;	mov [k], di			; just rewrite old fn with new fn

	ret

;=============================================================================
; :section:   Symbol
;-----------------------------------------------------------------------------
String.open_paren: db "(", 0
String.close_paren: db ")", 0
String.docstring: db "docstring", 0

Symbol.open_paren:
	mov a, String.open_paren
	callsave f.string0_to_list, a, b
	lst a
	callsave Symtab.fn, a, b
	sym a
	ret

Symbol.close_paren:
	mov a, String.close_paren
	callsave f.string0_to_list, a, b
	lst a
	callsave Symtab.fn, a, b
	sym a
	ret

Symbol.docstring:
	mov a, String.docstring
	callsave f.string0_to_list, a, b
	lst a
	callsave Symtab.fn, a, b
	sym a
	ret

;=============================================================================
	

