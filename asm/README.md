# Rabbit-vm #
# An approach to the Joy Programming Language. #

## NASM version ##

This is a working rabbit-vm written in NASM assembly language.

Currently development has shifted to FASM.

Hence, the rabbit source files to be found in src and local directories
are, even if developed with this NASM version, no longer compatible with it.