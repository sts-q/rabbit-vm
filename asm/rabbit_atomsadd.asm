; -*- mode: mynasm;-*-
;=============================================================================

; :file:   rabbit_atomsadd.asm		add atoms to symtab at rabbit startup

;=============================================================================
;   Rabbit-vm   An approach to the Joy Programming Language.
;   GNU GENERAL PUBLIC LICENSE, Version 3.0, http://www.gnu.org/licenses/
;   https://bitbucket.org/sts-q/rabbit-vm
;   Copyright (C) 2018 Heiko Kuhrt
;   Heiko.Kuhrt ( at ) yahoo.de
;=============================================================================
; :section:   macros			addatom: marco to add an atom into symtab
;-----------------------------------------------------------------------------
%macro addatom 3

	section .data
	%1.name 	: db %2, 0
	%1.docstring 	: db %3, 0
	
	section .text
	mov d, %1
	callsave Symtab.add, %1.name, %1.docstring
	%endmacro

;=============================================================================
; :chapter:   addatoms
;-----------------------------------------------------------------------------
section .text
Symtab.addatoms:

; -----------------------------------------------------------------------------
; :section:   stack shuffling words

addatom a.dup,		"dup", 		"x -- x x"
addatom a.dupd,		"dupd",		"x y -- x x y"
addatom a.2dup,		"2dup",		"x y -- x y x y"

addatom a.zap,		"zap", 		"x -- "
addatom a.2zap,		"2zap",		"x y -- "
addatom a.3zap,		"3zap",		"x y z -- "

addatom a.nip,		"nip", 		"x y -- y"
addatom a.2nip,		"2nip",		"x y z -- z"

addatom a.swap,		"swap",		"x y -- y x"
addatom a.swapd,	"swapd",	"x y z -- y x z"

addatom a.over,		"over",		"x y -- x y x"
addatom a.leap,		"leap",		"x y z -- x y z x"

addatom a.dip,		"dip",		"x p -- x  // exec p with x removed from stack, put x back afterwards"
addatom a.dip2,		"dip2",		"x y p -- x y"

addatom a.accent_1,	"^",		"x -- x  // hiccup word and dip it"
addatom a.accent_2,	"^^",		"x y -- x y  // hiccup word and dip2 it"


; -----------------------------------------------------------------------------
; :section:   push and pop upstack: an additional stack like freeputers software stack ss.

addatom a.nilup,	"nilup",	"  --    //  [  -- nil]  push nil to upstack"
addatom a.up,		"up",		"x --    //  [  -- x]    move x up to upstack"
addatom a.down,		"down",		"  -- x  //  [x --  ]    move x down from upstack"
addatom a.cpup,		"cpup",		"x -- x  //  [  -- x]    copy x to upstack"
addatom a.cpdown,	"cpdown",	"  -- x  //  [x -- x]    copy x from upstack"
addatom a.uzap,		"uzap",		"  --    //  [x --  ]    zap at upstack"
addatom a.consup,	"consup",	"x --    //  [l -- l]    cons x to list at upstack"
addatom a.unconsdown,	"unconsdown",	"  -- x  //  [l -- l]    uncons x from list at upstack"
addatom a.uinc,		"uinc",		"  --    //  [i -- i]    inc i at upstack"


; -----------------------------------------------------------------------------
; :section:   integer

addatom a.add,		"+", 		"i i -- i"
addatom a.sub,		"-", 		"i i -- i"
addatom a.mul,		"*", 		"i i -- i"
addatom a.div,		"/", 		"i i -- i"
addatom a.mod,		"mod", 		"i i -- i"
addatom a.divmod,	"divmod",	"i i -- i i"
addatom a.muldivmod,	"muldivmod",	"i i i -- i i"

addatom a.and,		"and",		"i i -- i"
addatom a.or,		"or",		"i i -- i"
addatom a.xor,		"xor",		"i i -- i"

addatom a.shl,		"shl",		"i i -- i  // shift left"
addatom a.shr,		"shr",		"i i -- i  // shift right"
addatom a.asr,		"asr",		"i i -- i  // arithmetic shift right"


addatom a.abs,		"abs",		"i -- i  // absolute i"
addatom a.sgn,		"sgn",		"i -- i  // sign of i"
addatom a.neg,		"neg",		"i -- i  // negate i: i := -i"
addatom a.inv,		"inv",		"i -- i  // invert all bits of i: xor i -1"


addatom a.inc,		"inc",		"i -- i"
addatom a.dec,		"dec",		"i -- i"
addatom a.shr1,		"2/",		"i -- i"
addatom a.shl1,		"2*",		"i -- i"


; -----------------------------------------------------------------------------
; :section:   boolean

addatom a.true,		"true",		"-- b  // true  == int 1"
addatom a.false,	"false",	"-- b  // false == int 0"
addatom a.not,		"not",		"x -- b  // x != 0  ->  0,  x == 0  ->  1"

; -----------------------------------------------------------------------------
; :section:   comparision

addatom a.comp_e,	"==",		"i i -- b"		
addatom a.comp_e_cs,	"==*",		"i j -- i b"		

addatom a.comp_g,	">",		"i i -- b"		
addatom a.comp_ge,	">=",		"i i -- b"		
addatom a.comp_ne,	"!=",		"i i -- b"		
addatom a.comp_le,	"<=",		"i i -- b"		
addatom a.comp_l,	"<",		"i i -- b"		

addatom a.comp_ze,	"0==",		"i -- b"

addatom a.cmp,		"cmp",		"x y -- i  // compare recursive: x<y -> -1   x=y -> 0   x>y -> 1"

addatom a.within,	"within?",	"i j k -- b  // return true if: j <= i < k "
addatom a.within_cs,	"within?*",	"i j k -- i b  // return true if: j <= i < k "

; -----------------------------------------------------------------------------
; :section:   type tests

addatom a.int?,		"int?",		"x -- b  // is x an integer?"
addatom a.sym?,		"sym?",		"x -- b  // is x a symbol?"
addatom a.lst?,		"lst?",		"x -- b  // is x a list?"

addatom a.int?cs,	"int?*",	"x -- x b  // is x an integer?"
addatom a.sym?cs,	"sym?*",	"x -- x b  // is x a symbol?"
addatom a.lst?cs,	"lst?*",	"x -- x b  // is x a list?"

addatom a.type?,	"type?",	"x u -- b  // is x of type u? (a 1 2 3)`a type? -> true, {}`docstring type? -> true"
addatom a.patt_cs,	"patt?*",	"l l -- b  // (1 2 3) ((int?) (int?))patt?* -> (1 2 3) true"


; -----------------------------------------------------------------------------
; :section:   lists

addatom a.cons,		"cons",		"x l -- l"
addatom a.uncons,	"uncons",	"l -- x l"
addatom a.unswons,	"unswons",	"l -- l x"
addatom a.swons,	"swons",	"l x -- l"
addatom a.swonsd,	"swonsd",	"l x y -- l y"
addatom a.conc,		"conc",		"l x -- l  // mutate last element of l to append x"
addatom a.join,		"join",		"l x -- l  // return new l with x appended. (uses return-stack, not tail-recursive)"

addatom a.copy,		"copy",		"l -- l  // return a copy of l"
addatom a.reverse,	"reverse",	"l -- l"
addatom a.rev,		"rev",		"l -- l  // reverse l in place"
addatom a.concat,	"concat",	"l l -- l"
addatom a.swoncat,	"swoncat",	"l l -- l  // swap concat"
addatom a.append,	"append",	"l l -- l  // mutate last element of l1 to append l2"
addatom a.equal,	"equal?",	"l l -- b  // compare with ==, not recursive"
addatom a.split,	"separate",	"l p -- l l  // apply predicate p to each elt, true -> l1  false -> l2"

addatom a.nil,		"nil",		"-- l  // empty list"
addatom a.nild,		"nild",		"x -- l x"
addatom a.is_nil,	"nil?",		"l -- b"
addatom a.is_nilcs,	"nil?*",	"l -- l b"
addatom a.is_nildcs,	"nild?*",	"l x -- l x b"

addatom a.first,	"first",	"l -- x"
addatom a.first_cs,	"first*",	"l -- l x"
addatom a.second,	"second",	"l -- x"
addatom a.third,	"third",	"l -- x"
addatom a.1drop,	"1drop",	"l -- l"
addatom a.elt,		"elt",		"l i -- x"

addatom a.solo,		"solo",		"x -- l"
addatom a.pair,		"pair",		"x y -- l"
addatom a.triple,	"triple",	"x y z -- l"
addatom a.quad,		"quad",		"w x y z -- l"
addatom a.unpair,	"unpair",	"l -- x y"
addatom a.untriple,	"untriple",	"l -- x y z"

addatom a.is_solo,	"solo?",	"l -- b"
addatom a.is_solo_cs,	"solo?*",	"l -- l b"

addatom a.unlist,	"unlist",	"l --  // uncons all elements of l to stack"


; -----------------------------------------------------------------------------
; :section:   invoke quotation

addatom a.wrd,		"`", 		"-- u  // put next item from program stream to stack."
addatom a.call,		"call", 	"x --  // call anything like ( x )i"
addatom a.i,		"i", 		"q -- "
addatom a.j,		"j", 		" -- "
addatom a.hiccup,	"hiccup",	" --  x // get and skip next instruction"
addatom a.return,	"return",		"// return immediately from running quotation"

; -----------------------------------------------------------------------------
; :section:   branching

addatom a.if,		"if", 		"x q p -- "
addatom a.if_rl,	"if/rl",	"x q p -- "
addatom a.ifnilcs_rl,	"ifnil*/rl",	"x q p -- x"
addatom a.ifnildcs_rl,	"ifnild*/rl",	"x q p -- x"
addatom a.iflezcs_rl,	"if0<=*/rl",	"i -- i"
addatom a.ife_rl,	"if==/rl",	"i i -- "
addatom a.ifl_rl,	"if</rl",	"i i -- "
addatom a.iflcs_rl,	"if<*/rl",	"i i -- "
addatom a.unless,	"unless",	"x q -- "

; -----------------------------------------------------------------------------
; :section:   definitions

addatom a.is_atom,	"atom?",	"x -- b"

addatom a.define,	"define", 	" u s q -- "

addatom a.nodef,	"nodef", 	" -- "
addatom a.nop,		"nop",		" -- "
addatom a.nodef2,	"nodef2",	" -- "


; -----------------------------------------------------------------------------
; :section:   stdin stdout

addatom a.print,	"print",	"l --  // print string"
addatom a.iprint,	"iprint",	"i --  // print integer"
addatom a.lprint,	"lprint",	"l --  // print list recursively. knows types: int wrd lst"
addatom a.spc,		"spc", 		" --   // print space"
addatom a.lf,		"lf",		" --   // print line-feed (newline)"

addatom a.readchr,	"readchr",	" -- c  // read char from stdin"


; -----------------------------------------------------------------------------
; :section:   VM

addatom a.exit,		"exit",		" --  // exit rabbit-vm"
addatom a.exit_failure,	"exitfail",	" --  // exit rabbit-vm with error-code 1"

addatom a.help,		"--help",	" --  // print short help"
addatom a.help_2,	"-h",		" --  // print short help"
addatom a.version,	"--version",	" --  // print version info"
addatom a.version_2,	"-v",		" --  // print version info"


; -----------------------------------------------------------------------------
; :section:   inner world modules

addatom a.symbol_sym,	"Symbol.sym",	"s -- u"
addatom a.symbol_name,	"Symbol.name",	"u -- s"
addatom a.symbol_docstring, "Symbol.docstring", "u -- s"
addatom a.symbol_def,	"Symbol.def",	"u -- q"
addatom a.symbol_source,"Symbol.source","u -- s i  // filename and line where symbol was first seen"
addatom a.symbol_list,	"Symbol.list",	"-- l  // list of all symbols"

addatom a.VM.version,	"VM.version",	" -- s  // version info"
addatom a.VM.mem,	"VM.mem",	" -- i i // bytes of ram used by VM:  bss-in-toto  list-elements"
addatom a.VM.time,	"VM.time",	" -- i i i //sys_clock_gettime/thread-cpu[sec][nanosec]   VM.uptime[sec]"

addatom a.list_gc,	"GC.run",	"-- i i  // return # of run, # of free list elements"

addatom a.parse_rabbit, "Parse.rabbit", "-- l  // parse sourcebuffer"

addatom a.Stacks.clear,	"Stacks.clear",	"--  // clear stack and upstack, not return-stack"
addatom a.Stacks.current,"Stacks.current"," -- i i i  // current usage of ds ss rs"
addatom a.Stacks.print, "Stacks.print",	" -- "
addatom a.Stacks.reset,	"Stacks.reset",	"\p --  // clear stack and upstack, reset return-stack, exec p"
addatom a.Stacks.size,	"Stacks.size",	" -- i i i  // size of ds ss rs"

; -----------------------------------------------------------------------------
; :section:   outer world modules

addatom a.Clock.msleep,	"Clock.msleep",	"i --  // sleep i milliseconds"

addatom a.file_fetch,	"File.fetch",	"s --  // read file s into sourcebuffer"
addatom a.file_store,	"File.store",	"s --  // write file s from sourcebuffer"
addatom a.file_exists,	"File.exists?",	"s -- b  // file exist?"
addatom a.file_unlink,	"File.unlink",	"s -- i  // remove file, return syscall exit code"
addatom a.file_chmod,	"File.chmod",	"s i -- i  // file-name mode: chmod of file, return syscall exit code"

addatom a.stdout_close,	"Stdout.close",	" -- "

addatom a.srcbu_print,	"Sourcebuffer.print",	" --  // print current sourcebuffer"
addatom a.srcbu_append,	"Sourcebuffer.append",	"s --  // CURRENTLY IT IS FILL, NOT APPEND"
addatom a.srcbu_content,"Sourcebuffer.content",	" -- s"

addatom a.stribu_print,	"Stringbuffer.print",	"--  // print current contents of Stringbuffer"
addatom a.stribu_clear,	"Stringbuffer.clear",	"--  // clear contents of Stringbuffer"
addatom a.stribu_append_char,	"Stringbuffer.append-char",	"c --  // append char to Stringbuffer"
addatom a.stribu_iscan,	"Stringbuffer.iscan",	"-- i  // read integer i from Stringbuffer"

addatom a.Time.ue,	"Time.ue",	" -- i  // seconds since Unix epoch: 1970/Jan/01 00:00 UTC"

addatom a.xterm_size,	"Xterm.size",	"-- i i  // ws_row ws_col"

addatom a.Sys_fork,	"Sys.fork",	"-- i  // pid"
addatom a.Sys_execve,	"Sys.execve",	"s sl sl --  // command args env"
addatom a.Sys_pipe2,	"Sys.pipe2",	"i -- i i i // flags -> fd-read fd-write errno"
addatom a.Sys_dup2,	"Sys.dup2",	"i i -- i -- old-fd new-fd  ->  fd|errno"
addatom a.Sys_fd_close,	"Sys.fd-close",	"i --  // close fd"
addatom a.Sys_fd_read,	"Sys.fd-read",	"i --  // read from fd into Sourcebuffer"
addatom a.Sys_fd_write,	"Sys.fd-write",	"i --  // write Sourcebuffer to fd"

addatom a.Sys_socket,	"Sys.socket",	"i i i -- i // domain type protocol  ->  socket-fd"
addatom a.Sys_connect,	"Sys.connect",	"i -- i  // 0|errno"

addatom a.Sys_waitid,	"Sys.waitid",	"i -- i  // child-pid -> child-exit-code, wait until childs exits"

; -----------------------------------------------------------------------------
ret

;=============================================================================
