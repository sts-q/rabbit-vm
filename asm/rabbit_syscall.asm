; -*- mode: mynasm;-*-
;=============================================================================

; :file:   rabbit_syscall.asm		Linux system calls

;=============================================================================
;   Rabbit-vm   An approach to the Joy Programming Language.
;   GNU GENERAL PUBLIC LICENSE, Version 3.0, http://www.gnu.org/licenses/
;   https://bitbucket.org/sts-q/rabbit-vm
;   Copyright (C) 2018 Heiko Kuhrt
;   Heiko.Kuhrt ( at ) yahoo.de
;=============================================================================

section .text

;=============================================================================
; :section:   stdin stdout
;-----------------------------------------------------------------------------
Syscall.stdin_init: ; ( --  // set stdin to nonblocking mode)	a
	syscall_4 SYS64_FCNTL, STDIN, F_SETFL, O_NONBLOCK
	call Syscall.time_ue
	mov [rabbit_start_time], a
	ret

Syscall.cprint:  ; ( al -- ) print one char to stdout 		a
	mov	[buffer], al
	syscall_4 SYS64_WRITE, STDOUT, buffer, 1
	ret

Syscall.print0: ; ( s -- ) print 0-terminated string  stdout	a
	mov b, a			; a := string-start   b := string-end

	jmp .while
    .loop:				; find end of string
	inc b				; current char pointer (cc^)
    .while:
    	mov 	dl, [b]
	_ {cmp 	dl, 0}, jnz .loop

	sub b, a
	syscall_4  SYS64_WRITE, STDOUT, a, b
	ret

Syscall.cread: ; ( -- c  // read char from stdin )		a 
	syscall_4 SYS64_READ, 0, buffer, 1
	cmp a, -11			; errno 11: EAGAIN Try again
	jz .nothing
	cmp a, 1
	jnz .error
	mov a, [buffer]
	ret
    .nothing:
    	mov a, 0			; return 0: no char found (try later again)
	ret
    .error:
    	mov a, -1
	ret

;=============================================================================
; :section:   files
;-----------------------------------------------------------------------------

Syscall.file_open:  ; ( str0:name -- fd )			a
	mov d, a				; k := name   ( in case of error )
	syscall_4  SYS64_OPEN, a, O_RDONLY, 0
	cmp a, 0
	jle .error
	ret 					; a := fd

    .error:
    	error `File.read: open failed.\n\rfile-name: `, d

Syscall.file_read:  ; ( fd -- fd i:bytes  // error if failure )  a
	mov d, a				; save fd
	syscall_4  SYS64_READ, a, source_buffer, source_buffer_size
	cmp	a, source_buffer_size
	jge .error
	cmp 	a, 0
	jl .error
	mov [source_buffer + a], byte 0		; terminate source string with 0
	mov b, a
	mov a, d 				; return ( fd bytes )
	ret
    .error:
    	error "File.read: reading failed."

Syscall.file_close:  ; ( fd --  // close file )			a
	syscall_4  SYS64_CLOSE, a, 0,0
	cmp a, 0
	js .error
	ret

    .error:
    	error "Syscall.file_close failed."

Syscall.file_load: ; ( str0:name -- // read file into sourcebuffer ) a
	call Syscall.file_open			; ( name -- fd     )
	call Syscall.file_read			; ( fd -- fd bytes )
	mov [source_buffer_current], b
	call Syscall.file_close			; ( fd -- 	   )
	ret

Syscall.file_unlink:  ; ( str0:file-name -- errno  // unlink (remove) file )	a
	syscall_4 SYS64_UNLINK, a, 0,0
	ret

Syscall.file_chmod:  ; ( str0:file-name  i:mode -- errno  // chcmod of file )
	syscall_4 SYS64_CHMOD, a, b, 0
	ret

; ----------------------------------------------
Syscall.file_store:  ; ( str0:name  -- // write Sourcebuffer to file name )
	mov b, O_WRONLY
	or  b, O_CREAT
	or  b, O_TRUNC
	mov d, 420
	syscall_4 SYS64_OPEN, a, b, d
	_ {cmp a, zero}, jl .err_open_store

	mov k, a
	call Sourcebuffer.current
	dec a					; without trailing zero
	mov d, a
	_ {cmp d, source_buffer_size}, jge .err_strange_length
	syscall_4 SYS64_WRITE, k, source_buffer, d
	_ {cmp a, zero}, jl .err_store

	mov a, k
	call Syscall.file_close
	ret
    .err_open_store:
    	mov [buffer], zero
    	error "File.store open failed.", buffer, a
    .err_strange_length: error "File.store: strange sourcebuffer length"
    .err_store: error "File.store: failed."
	

Syscall.file_is: ; ( str0:name -- 0|1 )				a
	syscall_4 SYS64_OPEN, a, O_RDONLY, 0
	cmp a, 0
	jle .no
    .yes:
    	syscall_4 SYS64_CLOSE, a, 0, 0
    	mov a, 1
	ret
    .no:
    	mov a, 0
	ret




;=============================================================================
; :section:   others
;-----------------------------------------------------------------------------

Syscall.return_stack_size: ; ( -- i )				a
	mov [buffer], zero
	syscall_4 SYS64_GETRLIMIT, RLIMIT_STACK, buffer, zero
	mov a, [buffer]
	shr a, 3				; bytes -> stack-items
	ret

	
;=============================================================================
Syscall.time_ue: ; ( -- unix-epoch-seconds )			a
	syscall_4 SYS64_TIME, 0,0,0
	ret


;=============================================================================
Syscall.clock_time:  ; ( -- i:sec i:nanosec   // THREAD_CPUTIME)  a
	clr a
	mov [buffer],      a
	mov [buffer + ws], a
	syscall_4 SYS64_CLOCK_GETTIME, CLOCK_THREAD_CPUTIME_ID, buffer, 0
	_ {cmp a, zero}, jne .err
	mov a, [buffer]					; ?
	mov b, [buffer + ws]				; long
	ret
    .err: error "Syscall.clock_time failed."


Syscall.clock_msleep: ; ( milliseconds -- )			a
	mov b, 1000
	cqo
  	idiv b					; a := sec
	mov b, d
	imul b, 1000000				; b := nanosec
Syscall.clock_nanosleep: ; ( seconds nanoseconds -- )		a
	mov [buffer], a
	mov [buffer+ws],  b
	syscall_4 SYS64_NANOSLEEP, buffer, buffer, 0
	ret
	
;=============================================================================
; linux/arch/alpha/include/uapi/asm/termios.h
;struct winsize {
;	unsigned short ws_row;
;	unsigned short ws_col;
;	unsigned short ws_xpixel;
;	unsigned short ws_ypixel;
; };

Syscall.xterm_size: ; ( -- ws_row ws_col )			a
	mov [buffer],      zero
	mov [buffer + ws], zero
	syscall_4 SYS64_IOCTL, STDIN, TIOCGWINSZ, buffer
	clr a, b
	mov ax, [buffer]
	mov bx, [buffer + 2]
	ret

	
;=============================================================================
; :section:   System
;-----------------------------------------------------------------------------
Syscall.sys_fork:
	syscall_4 SYS64_FORK, 0,0,0
	_ {cmp a, zero}, jl .err
	ret

    .err:
	mov [buffer], zero
	error `Sys.fork failed.errno`, buffer, a


Syscall.sys_execve:  ; ( *str0:file-name  *argv[]  *env{}  -- )
	syscall_4 SYS64_EXECVE, a, b, d
	_ {cmp a, zero}, jl .err
	ret

    .err:
	mov [buffer], zero
	error `Sys.execve failed.\nerrno`, buffer, a

Syscall.pipe2:  ; ( i:flags --  i:errno fd fd // open pipe, uses buffer )
	mov k, a
	clr a
	mov [buffer], a
	mov [buffer + ws] , a
	syscall_4 SYS64_PIPE2, buffer, k, 0
	clr b,d
	mov ebx, [buffer]
	mov edx, [buffer + 4]
	ret

Syscall.dup2:  ; ( i:old-fd i:new-fd -- i:new-fd|errno )
	syscall_4 SYS64_DUP2, a, b, 0
	ret

Syscall.fd_close:  ; ( i:fd --   // close fd )
	call Syscall.file_close
	ret

Syscall.socket:  ; ( i:family i:type i:protocol -- i:socket-fd|errono )
	syscall_4 SYS64_SOCKET, a, b, d
	ret

;make-serv-addr {}
;	AF_INET `addr     !b
;	port    `addr 2 + !2b
;    .!2b {i a --}  ( 256 /% )dip  dup inc^ !b^ !b  ; lscpu shows endianness: little-endian ???

;/* Structure describing an Internet (IP) socket address. */
;#if  __UAPI_DEF_SOCKADDR_IN
;#define __SOCK_SIZE__	16		/* sizeof(struct sockaddr)	*/
;struct sockaddr_in {
;  __kernel_sa_family_t	sin_family;	/* Address family		*/  unsigned short
;  __be16		sin_port;	/* Port number			*/  typedef __u16 __bitwise __be16;
;  struct in_addr	sin_addr;	/* Internet address		*/

;/* Internet address. */
;struct in_addr {
;	__be32	s_addr;
;};

Syscall.connect: ; ( i:socket-fd i:port -- 0|errno )
	mov [socket_buffer], zero
	mov [socket_buffer + ws], zero
	mov [socket_buffer + 2 * ws], zero
	
	mov [socket_buffer],     byte AF_INET

	mov [socket_buffer + 3], byte bl	; htons: net-byte-order
	shr b, 8
	mov [socket_buffer + 2], byte bl
	
	mov [socket_buffer + 4], byte 127
	mov [socket_buffer + 5], byte 0
	mov [socket_buffer + 6], byte 0
	mov [socket_buffer + 7], byte 1

	syscall_4 SYS64_CONNECT, a, socket_buffer, 16
	ret


Syscall.waitid:  ; ( child-pid -- child-exit-code )
	save_regs
	push a
	
	mov [buffer     ], zero
	mov [buffer + ws], zero
	mov [buffer + 16], zero
	mov [buffer + 24], zero
	mov [buffer + 32], zero
	
	mov rax, SYS64_WAITID
	mov rdi, P_PID				; which:	idtype_t idtype
	pop rsi					; pid		id_t id
	mov rdx, buffer				; info		siginfo_t *infop
	mov r10, WEXITED			; options	int options
	mov r8,  0				; usage		struct rusage *ru
	mov r9,  0
;	RAX, RDI, RSI, RDX, RCX, R8, R9		; obviously wrong...
; 	rax rdi rsi rdx r10 r8 f9		; probably better
	syscall
	load_regs
	clr a
;	_ {mov eax, [buffer + 0]}, iprint_ a		; 17		SIGCHLD
;	_ {mov eax, [buffer + 4]}, iprint_ a		;  0
;	_ {mov eax, [buffer + 8]}, iprint_ a		;  1		CLD_EXITED ???
;	_ {mov eax, [buffer + 12]}, iprint_ a		;  0
;	_ {mov eax, [buffer + 16]}, iprint_ a		; pid
;	_ {mov eax, [buffer + 20]}, iprint_ a		; 1000		uid ???
;	_ {mov eax, [buffer + 24]}, iprint_ a		; probably exit-code of child: 0 1 2
;	_ {mov eax, [buffer + 28]}, iprint_ a
;	xx
	mov eax, [buffer + 24]
	ret

;=============================================================================
; :section:   exit
;-----------------------------------------------------------------------------

exit: 	syscall_4 SYS64_EXIT, 0, 0, 0
exit_1: syscall_4 SYS64_EXIT, 1, 0, 0

;=============================================================================
