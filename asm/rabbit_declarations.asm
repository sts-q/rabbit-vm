; -*- mode: mynasm;-*-
;=============================================================================

; :file:   rabbit_declarations.asm	nasm-%define, section .bss and .data, global macros
	
;=============================================================================
;   Rabbit-vm   An approach to the Joy Programming Language.
;   GNU GENERAL PUBLIC LICENSE, Version 3.0, http://www.gnu.org/licenses/
;   https://bitbucket.org/sts-q/rabbit-vm
;   Copyright (C) 2018 Heiko Kuhrt
;   Heiko.Kuhrt ( at ) yahoo.de
;=============================================================================
; :chapter:   renaming registers and global constants
;----------------------------------------------------------------------------
; :section:   constants

%define ws		 8			; word size
%define KiB	      1024
%define off		 0
%define on		 1

%define SYS64_READ	 0
%define SYS64_WRITE	 1
%define SYS64_OPEN	 2
%define SYS64_CLOSE	 3
%define SYS64_IOCTL	16
%define SYS64_DUP2	33
%define SYS64_NANOSLEEP	35
%define SYS64_SOCKET	41
%define SYS64_CONNECT	42
;sys64_accept   {} 43
;sys64_bind     {} 49
;sys64_listen   {} 50
;sys64_fcntl    {} 72
%define SYS64_FORK	57
%define SYS64_EXECVE	59
%define SYS64_EXIT	60
%define SYS64_FCNTL	72
%define SYS64_UNLINK	87
%define SYS64_CHMOD	90
%define SYS64_GETRLIMIT	97
%define SYS64_TIME     201
%define SYS64_CLOCK_GETTIME 228
%define SYS64_WAITID   247
%define SYS64_PIPE2    293


;%define SYS_WRITE	 4

%define STDIN	 	 0
%define STDOUT	 	 1

%define F_GETFL		 3
%define F_SETFL		 4

%define O_RDONLY 	 0
%define O_WRONLY 	 1
%define O_NONBLOCK    2048

%define TIOCGWINSZ  0x5413		; linux/include/uapi/asm-generic/ioctls.h
%define RLIMIT_STACK	 3
%define CLOCK_THREAD_CPUTIME_ID 3	; https://elixir.free-electrons.com/linux/latest/source/include/uapi/linux/time.h

%define O_CREAT		   0x40	
%define O_TRUNC		 0x0200

; socket
%define SOCKLEN_T.ADDRLEN   24
%define AF_INET              2

; WAITID
%define P_ALL		0
%define P_PID		1
%define P_PGID		2
%define WEXITED		4


;----------------------------------------------------------------------------
; :section:   registers

; aregs
%define a	rax	; first function parameter  / return value
%define b	rbx	; second function parameter / return value
%define d	rdx	; third function parameter

; fregs			; function regs
%define c	rcx	; counter
%define si	rsi	; source index
%define di	rdi 	; destination index
%define k  	r9	; key
%define v	r8	; value

; gregs			; global regs
;define rsp	rsp	; return-stack pointer                              
%define dsp	rbp	; data-stack pointer      
%define ssp	r10	; software-stack pointer ( upstack )
%define sstos	r11	; top of software-stack

%define tos	r12	; top of data-stack	
%define sos	r13	; second of data-stack	
%define prog	r14	; currently executing quotation
%define zero	r15	; just and always zero         

;=============================================================================
; :chapter:   macros
;-----------------------------------------------------------------------------

%macro _ 1-*
	%rep  %0 
		%1
		%rotate 1
	%endrep
	%endmacro

%define gensym_counter 1
%macro gensym_next 0
	%assign gensym_counter gensym_counter + 1
	%define gensym   .gs_ %+ gensym_counter
	%endmacro

%macro clr 1-5
	%rep %0
		xor %1, %1
		%rotate 1
	%endrep
	%endmacro

%macro zero? 2
	cmp %1, zero
	je %2
	%endmacro
%macro notzero? 2
	cmp %1, zero
	jne %2
	%endmacro

%macro rdrop 0
	add rsp, ws
	%endmacro
	
%macro save_regs 0
	push rbx
	push rcx
	push rdx
	push rsi
	push rdi
	push r8
	push r9
	push r10
	push r11
	%endmacro
%macro load_regs 0		; not:  a  tos sos prog zero  dsp rsp
	pop r11			; sstos
	pop r10			; ssp
	pop r9			; k
	pop r8			; v
	pop rdi			; di
	pop rsi			; si
	pop rdx			; d
	pop rcx			; c
	pop rbx			; b
	%endmacro
	
%macro syscall_4 4
	; rax rdi rsi rdx r10 r8 f9
	save_regs
	mov rdi, %2
	mov rsi, %3
	mov rax, %1
	mov rdx, %4
	syscall
	load_regs
	%endmacro

%macro callsave 1-4 a,b,d
	push c
	push d
	push si
	push di
	push k
	push v

	%ifnidn %2,a
		mov a, %2
		%endif
	%ifnidn %3,b
		mov b, %3
		%endif
	%ifnidn %4,d
		mov d, %4
		%endif
	call %1

	pop v
	pop k
	pop di
	pop si
	pop d
	pop c
	%endmacro

%macro callfun 1-10 a,b,d		; parameter a b d    push max 6 regs

	%if %0 > 4			; don't push anything with < 3 parameters
	    %rep 4				; rotate to push-1
		%rotate 1
	    %endrep
	    %rep %0 - 4				; push
		push %1
		%rotate 1
	    %endrep
	    %rep %0 - 4				; rotate back
		%rotate -1
	    %endrep
	    %rep 4				; rotate back
		%rotate -1
	    %endrep
	%endif

	%ifnidn %2,a			; %2	a := first parameter
		mov a, %2
		%endif
	%ifnidn %3,b			; %3	b := second parameter
		mov b, %3
		%endif
	%ifnidn %4,d			; %4	d := third parameter
		mov d, %4
		%endif
	call %1

	%if %0 > 4
	    %rep %0 - 4
		%rotate -1			; rotate to last push
		pop %1
	    %endrep
	%endif

	%endmacro

;=============================================================================
; :chapter:   data                              constants compiled into binary
; :chapter:   bss   blocks started by symbol    statically allocated variables
;-----------------------------------------------------------------------------
section .bss
bss_first:					; first position of bss, used by VM.mem

;-----------------------------------------------------------------------------
; :section:   VM
section .data

initial_source: 		db "<NN>", 0	; written by Symtab.init to CurrentSource

mess_commandline:		db "<commandline>", 0
mess_gc_now_long:		db `\n-------  gc-now  -------\n`, 0
mess_gc_now_short:		db `*\n`, 0
mess_error:			db `\r\n\n############ error\r\n`, 0

vm_errorfun_name: 		db "VM.error", 0



%defstr PLATFORM_STR  	__OUTPUT_FORMAT__
%strcat VERSION_STR 	`Rabbit-vm  2018-January  rabbit-vm-asm-x86-64  ` PLATFORM_STR "  " __DATE__ "  " __TIME__
mess_version:		db VERSION_STR, 0


;-----------------------------------------------------------------------------
section .bss

socket_buffer: 		 	resb SOCKLEN_T.ADDRLEN

rabbit_start_time: 		resq 1		; i   	Time.ue

argc:				resq 1		; i   	number of command line args
argv:				resq 1		; addr 	**char

CurrentSource:	 		resq 1		; lst	currently parsed source-file-name
CurrentLine:			resq 1		; i	currently parsed source-code-line


%define buffer_size		16 * KiB	;	buffer pro diversa: syscalls
%define buffer_last		buffer + buffer_size - 1
buffer: 			resb buffer_size

%define source_buffer_size	1024 * KiB	;	buffer for source or string to parse
%define source_buffer_last  	source_buffer + source_buffer_size - 2	; 1 byte for terminating 0
source_buffer_current:		resq 1		; i	first free pos, if filled point to trail. zero
source_buffer:			resb source_buffer_size

%define string_buffer_size	64 * KiB	; 	buffer for strings: print0, source_file_name
string_buffer_current:		resq 1
string_buffer:			resb string_buffer_size


;-----------------------------------------------------------------------------
; :section:   types

%define TYPE_MASK	3
%define TYPE_UNMASK    -4
%define TYPE_SHIFT	2
%define INT		0
%define SYM		1
%define LST		2
%define ARR		3

%define nil           LST
%define one		1 << 2
%define minus_one      -1 << 2


;-----------------------------------------------------------------------------
; :section:   stacks

%define WALL		qword 1111222233334444

%define stackspace	256
		alignb 8
border_1:	resq 1
dstack:		resq stackspace		; 	data-stack ds, well, just the stack!
border_2:	resq 1
sstack:		resq stackspace		;	software-stack ss, second stack, rabbit: upstack
border_3:	resq 1

rs_base:	resq 1			;	x86 return-stack, rsp at startup, used by Stacks.reset, Stacks.current


;-----------------------------------------------------------------------------
; :section:   symtab

%define symtab_maximal		5000
%define symtab_last		symtab + (symtab_maximal - 1) * tFunc_size

struc tFunc

	o_fn: 		resq 1		; addr:fn
	o_name: 	resq 1		; lst
	o_docstring:	resq 1		; lst
	o_pSrc:		resq 1		; lst		<--> CurrentSource
	o_line:		resq 1		; i		<--> CurrentLine
	
endstruc


section .bss
alignb 8
symtab_ff:			resq 1	; first free entry
symtab:				resb symtab_maximal * tFunc_size


;-----------------------------------------------------------------------------
; :section:   parser

section .data
word_characters:		db "-.abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_+*/<>=?!~@§$%&\[]", 0

mess_parsing_failed:		db "#######  Parse.rabbit: parsing failed.", 0
mess_strange_char_found:	db "#######  Parse.rabbit: strange char found.", 0
mess_closing_paren_missing: 	db "#######  Parse.rabbit: closing paren missing", 0
mess_open_paren_missing: 	db "#######  Parse.rabbit: open paren missing.", 0
mess_string_terminator_missing: db "#######  Parse.rabbit: string terminator missing.", 0


section .bss
Parse.source_file_name: resq 1		; addr	*str0		for parsers errmessing, (CurrentSource is a lst)

Parse.rs_base:		resq 1		; position of rsp starting Rabbit.parse


;-----------------------------------------------------------------------------
; :section:   lists

%define		maximal_list_elements	4 * KiB * KiB

section .bss
alignb 16
list_mem:	resb maximal_list_elements * 16
list_free:	resq 1				; root of list of free elements  XXX don't move XXX


;-----------------------------------------------------------------------------
; :section:   gc

%define GC_CHECKS	on

%define GC_BIT		31

section .bss

List.gc.runs:		resq 1

List.gc.temp_si:	resq 1
List.gc.temp_di:	resq 1
List.gc.temp_k:		resq 1
List.gc.temp_v:		resq 1
List.gc.temp_c:		resq 1
List.gc.temp_d:		resq 1



;=============================================================================

bss_last:

;=============================================================================


