; -*- mode: mynasm;-*-
;=============================================================================

; :file:   rabbit_stack.asm		stack operations, exec quotation on stack

;=============================================================================
;   Rabbit-vm   An approach to the Joy Programming Language.
;   GNU GENERAL PUBLIC LICENSE, Version 3.0, http://www.gnu.org/licenses/
;   https://bitbucket.org/sts-q/rabbit-vm
;   Copyright (C) 2018 Heiko Kuhrt
;   Heiko.Kuhrt ( at ) yahoo.de
;=============================================================================
; :section:   stack macros
;-----------------------------------------------------------------------------
error_ds_underflow: 	error "messfail_ds_underflow"
error_ds_overflow: 	error "messfail_ds_overflow"
error_ss_underflow: 	error "messfail_ss_underflow"
error_ss_overflow: 	error "messfail_ss_overflow"

error_exec_only_lists:	error "i: list on stack expected."

; -----------------------------------------------------------------------------
%macro dspNext 0
	sub dsp, ws				 
	cmp dsp, dstack
	js  error_ds_overflow
	%endmacro

%macro dspPrev 0
	add dsp, ws
	cmp dsp, border_2
	jg error_ds_underflow
	%endmacro

%macro sspNext 0
	sub ssp, ws				 
	cmp ssp, border_2
	jle  error_ss_overflow
	%endmacro

%macro sspPrev 0
	add ssp, ws
	cmp ssp, border_3
	jg error_ss_underflow
	%endmacro
; -----------------------------------------------------------------------------

%macro up 1
	dspNext
	mov [dsp], %1
	%endmacro
%macro down 1
	mov %1, [dsp]
	dspPrev
	%endmacro

%macro shiftup 1
	up sos
	mov sos, tos
	mov tos, %1
	%endmacro
	
%macro shiftdown 1
	mov %1, tos
	mov tos, sos
	down sos
	%endmacro

;=============================================================================
; :section:   exec			execute quotation on stack ( next )
; low level funs have more atoms than list, int, def or string
; atoms get a lot more executed than defs
;-----------------------------------------------------------------------------

%macro exec 0 ; ( l --  // exec l on stack )
    	_ {cmp prog, zero}, je %%done

    %%loop:


    	mov d,    [prog]		; this quotation-element-value: int|sym|lst
;	mov b,    prog			; current program as parameter to atom: nodef2
	mov prog, [prog + ws]		; next quotation-element
	mov al, dl			; notsym?
	and al, TYPE_MASK
	cmp al, SYM
	jnz %%just_cons			; not a symbol  ->  just cons: int|lst
	sub dl, al
;	cmp d, last_atom_address
	cmp d, dsp
	jg %%recur_on_list		; symbol points to list == is a def
    %%call_atom:
	jmp d				; symbol points to atom  ->  call atom

    %%recur_on_list:
	mov a, d
	call f.execute
	cmp prog, zero			; next?
	jnz %%loop
	pop prog
  unlst prog
	ret
	
    %%just_cons:
	up sos
	mov sos, tos
	mov tos, d
    	cmp prog, zero			; next?
	jnz %%loop
    %%done:
	pop prog
  unlst prog
	ret
	%endmacro
	
; -----------------------------------------------------------------------------
f.execute: ; ( l -- // exec l on stack )
  lst prog
	push prog
	mov prog, a
	exec

; -----------------------------------------------------------------------------
%macro goto_exec 0
	jmp exec_code
	%endmacro

exec_code:
	exec
	
; -----------------------------------------------------------------------------
%macro exec_x 1				; execute x like ( x )i
	mov v, %1
	List.link  k,  v, zero,  err_exec_x_ll_failed
	mov a, k
	call f.execute
	%endmacro

err_exec_x_ll_failed: error "exec_x: List.link failed."
;=============================================================================
; :section:   atom macros
;-----------------------------------------------------------------------------
%macro lit 1
	up sos
	mov sos, tos
	mov tos, %1
	%endmacro
%macro liti 1
	lit %1
	int tos
	%endmacro
%macro lits 1
	lit %1
	sym tos
	%endmacro
%macro litl 1
	lit %1
	lst tos
	%endmacro

; -----------------------------------------------------------------------------
%macro dup 0
	up sos
	mov sos, tos
	%endmacro
%macro swap 0
	xchg tos, sos
	%endmacro
%macro zap 0
	mov tos, sos
	down sos
	%endmacro
%macro nip 0
	down sos
	%endmacro
%macro i.add 0
	add tos, sos
	down sos
	%endmacro
%macro i.mul 0
	mov a, sos
	uti tos
	uti a
	imul tos
	mov tos, a
	int tos
	down sos
	%endmacro
%macro eq 0
	xor a,a
	cmp tos, sos
	mov b, one
	cmovz a, b
	down sos
	mov tos, a
	%endmacro
	
;=============================================================================
; :section:   software stack
;-----------------------------------------------------------------------------
%macro spush 0
	sspNext
	mov [ssp], sstos
	mov sstos, tos
	mov tos, sos
	down sos
	%endmacro
	
%macro spop 0
	up sos
	mov sos, tos
	mov tos, sstos
	mov sstos, [ssp]
	sspPrev
	%endmacro

;=============================================================================
; :section:   stack init etc
;-----------------------------------------------------------------------------
Stacks.init:
	mov   	dsp, border_2		; init DS
	mov   	ssp, border_3		; init SS
	mov	[rs_base], rsp		; store initial return-stack-base

	mov a, WALL
	mov [border_1], a
	mov [border_2], a
	mov [border_3], a

	xor	tos, tos
	xor	sos, sos
	xor 	sstos, sstos

	ret	

error_stack_borders_hurt:
	lf
	strprint "messfail_stack_borders_hurt"
	lf
	jmp exit_1

Stacks.check_borders:
	mov b, WALL
	mov a, [border_1]
	  cmp a, b
	  jnz error_stack_borders_hurt
	mov a, [border_2]
	  cmp a, b
	  jnz error_stack_borders_hurt
	mov a, [border_3]
	  cmp a, b
	  jnz error_stack_borders_hurt
	ret

Stacks.clear:
	call Stacks.check_borders
	
	xor	tos, tos
	xor	sos, sos
	xor	sstos, sstos

	mov   	dsp, border_2		; init DS
	mov   	ssp, border_3		; init SS
	; but not return stack!

	ret
	
; -----------------------------------------------------------------------------
Stacks.ds_load:
	mov a, border_2
	sub a, dsp
	shr a, 3
	ret
	
Stacks.ss_load:
	mov a, border_3
	sub a, ssp
	shr a, 3
	ret
	
Stacks.rs_load:
	mov a, [rs_base]
	sub a, rsp
	shr a, 3
	ret

Stacks.print_do: ; ( c d si -- )
	jmp .while

    .loop:
	callsave  f.xprint, [si], b
	add si, ws
	dec c
    .while: _ {cmp c, zero}, jg .loop
    
	ret
	
Stacks.print:
	call Stacks.ds_load
	mov c, a
	_ iprint c, strprint " DS "
	_ {cmp c, 1}, jl .ds_done
	callsave f.xprint, tos, b
	_ {cmp c, 2}, jl .ds_done
	callsave f.xprint, sos, b
    	sub c, 2
	mov si, dsp
	call Stacks.print_do
    .ds_done:
	lf
	
	call Stacks.ss_load
	mov c, a
	_ iprint c, strprint " ss "
	_ {cmp c, 1}, jl .ss_done
	callsave f.xprint, sstos, b
	dec c
	mov si, ssp
	call Stacks.print_do
    .ss_done:
	lf

	call Stacks.rs_load
	_ iprint_ a, cprint 'R', cprint 'S', spc
	lf
	ret

%macro stacks 0
	callsave Stacks.print, a, b
	%endmacro
;=============================================================================
