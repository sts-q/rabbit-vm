; -*- mode: mynasm;-*-
;=============================================================================

; :file:   rabbit_types.asm		types built into Rabbit-vm: list symbol integer

;=============================================================================
;   Rabbit-vm   An approach to the Joy Programming Language.
;   GNU GENERAL PUBLIC LICENSE, Version 3.0, http://www.gnu.org/licenses/
;   https://bitbucket.org/sts-q/rabbit-vm
;   Copyright (C) 2018 Heiko Kuhrt
;   Heiko.Kuhrt ( at ) yahoo.de
;=============================================================================
; :section:   type-checks
;-----------------------------------------------------------------------------
%macro int 1
	shl %1, TYPE_SHIFT
	%endmacro
%macro sym 1
	add %1, SYM
	%endmacro
%macro lst 1
	add %1, LST
	%endmacro
%macro arr 1
	add %1, ARR
	%endmacro
; --------------------------------------------------
%macro int? 2
	mov a, %1
	and a, TYPE_MASK
;	cmp a, INT
	jz %2
	%endmacro
%macro notint? 2
	mov a, %1
	and a, TYPE_MASK
;	cmp a, INT
	jnz %2
	%endmacro

%macro expectint 1-2
	notint? %1, error_int_expected
	%if %0 = 2
	    notint? %2, error_int_expected
	%endif
	%endmacro

%macro sym? 2
	mov a, %1
	and a, TYPE_MASK
	cmp a, SYM
	jz %2
	%endmacro
%macro notsym? 2
	mov a, %1
	and a, TYPE_MASK
	cmp a, SYM
	jnz %2
	%endmacro

%macro lst? 2
	mov a, %1
	and a, TYPE_MASK
	cmp a, LST
	jz %2
	%endmacro
%macro notlst? 2
	mov a, %1
	and a, TYPE_MASK
	cmp a, LST
	jnz %2
	%endmacro
%macro nil? 2
	cmp %1, nil
	jz %2
	%endmacro
%macro notnil? 2
	cmp %1, nil
	jnz %2
	%endmacro

%macro arr? 2
	mov a, %1
	and a, TYPE_MASK
	cmp a, ARR
	jz %2
	%endmacro
%macro notarr? 2
	mov a, %1
	and a, TYPE_MASK
	cmp a, ARR
	jnz %2
	%endmacro

%macro wall? 2
	mov b, WALL
	_ {cmp %1, b}, je %2
	%endmacro

;; --------------------------------------------------
%macro uti 1				; untype integer: int back to raw x86-integer
	sar %1, TYPE_SHIFT
	%endmacro
%macro unlst 1
	and %1, TYPE_UNMASK
	%endmacro
%macro unsym 1
	and %1, TYPE_UNMASK
	%endmacro
%macro unarr 1
	and %1, TYPE_UNMASK
	%endmacro

	
;=============================================================================
; :section:   print
;-----------------------------------------------------------------------------
f.xprint:  ; ( x --  // print x: integer, ord, list of array )
	mov k, a
	int? k,	.print_int
	sym? k, .print_sym
	lst? k, .print_lst
	arr? k, .print_arr
	error "f.xprint: something went wrong."

    .print_int:
    	uti k
    	iprint_ k
	ret

    .print_sym:
    	unsym k
	callsave Symtab.symbol, k
	unlst a
	callsave f.list_print, a
	spc
	ret

    .print_lst:
    	nil? k, .print_nil
	push tos
    	  cprint '('
 	  mov tos, k
	  call i.length
	  uti tos
    	  iprint tos
    	  cprint ')'
	pop tos
	spc
	ret
    .print_nil:
   	strprint "nil "
	ret

    .print_arr:
    	unarr k
    	iprint k
    	cprint 'A'
	spc
	ret

%macro xprint 0
	callsave f.xprint, tos
	mov tos, sos
	down sos
	%endmacro
; -----------------------------------------------------------------------------
f.lprint:  ; ( l --  // print list recursive )
	mov si, a
	notlst? si, .no_list
	unlst si
	cprint '('

    .while zero? si, .done
    	mov d,  [si]
	mov si, [si + ws]
	lst? d, .print_list
	
    .print_others:	
	callsave f.xprint, d
	jmp .while
    .print_list:
    	callsave f.lprint, d
	jmp .while
	
    .done:
	cprint ')'
	ret
	
    .no_list: error "lprint: list on stack expected."

%macro lprint 0
	callsave f.lprint, tos
	mov tos, sos
	down sos
	%endmacro
; -----------------------------------------------------------------------------

