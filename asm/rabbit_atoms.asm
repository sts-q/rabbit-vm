; -*- mode: mynasm;-*-
;=============================================================================

; :file:   rabbit_atoms.asm		define all atoms build into rabbit-vm

;=============================================================================
;   Rabbit-vm   An approach to the Joy Programming Language.
;   GNU GENERAL PUBLIC LICENSE, Version 3.0, http://www.gnu.org/licenses/
;   https://bitbucket.org/sts-q/rabbit-vm
;   Copyright (C) 2018 Heiko Kuhrt
;   Heiko.Kuhrt ( at ) yahoo.de
;=============================================================================
; :section:   macros			macro to define an atom
;-----------------------------------------------------------------------------
%macro def 1
	align 8
	%1 
	%endmacro
;=============================================================================
first_atom_address:
;=============================================================================
; :chapter:   define atoms
; :section:   stack
;-----------------------------------------------------------------------------
def a.dup:
	dup
	exec

def a.dupd:
	up sos
	exec

def a.2dup:
	up sos
	up tos
	exec

def a.zap:
	zap
	exec

def a.2zap:
	down tos
	down sos
	exec

def a.3zap:
	down tos
	down tos
	down sos
	exec

def a.nip:
	nip
	exec

def a.2nip:
	down sos
	down sos
	exec

def a.swap:
	xchg tos, sos
	exec

def a.swapd:
	mov a, [dsp]
	mov [dsp], sos
	mov sos, a
	exec

def a.over:
	mov a, sos
	shiftup a
	exec

def a.leap:
	mov a, [dsp]
	shiftup a
	exec

def a.dip:
	notlst? tos, .err_list_expected
	mov v, tos
	unlst v
	push sos
	down tos
	down sos
	mov a, v
	call f.execute
	pop b
	shiftup b 
	exec
    .err_list_expected: error "dip: quotation expected"
	
def a.dip2:
	notlst? tos, .err_list_expected
	mov v, tos
	unlst v
	push sos
	down sos
	push sos
	down tos
	down sos
	mov a, v
	call f.execute
	pop b
	shiftup b 
	pop b
	shiftup b 
	exec
    .err_list_expected: error "dip2: quotation expected"

def a.accent_1:
	_ {cmp prog, zero}, jz .err_nil
	mov v,    [prog]
	mov prog, [prog + ws]
	shiftdown a
	push a

	exec_x v

	pop a
	shiftup a
	exec
    .err_nil: error "^: Running program is empty."

def a.accent_2:
	_ {cmp prog, zero}, jz .err_nil
	mov v,    [prog]
	mov prog, [prog + ws]
	push tos
	push sos
	down tos
	down sos

	exec_x v

	up sos
	up tos
	pop sos
	pop tos
	exec
    .err_nil: error "^^: Running program is empty."

	
;============================================================================
; :section:   upstack
;-----------------------------------------------------------------------------
def a.up:
	spush
	exec

def a.down:
	spop
	exec

def a.nilup:
	sspNext
	mov [ssp], sstos
	mov sstos, nil
	exec

def a.cpup:
	sspNext
	mov [ssp], sstos
	mov sstos, tos
	exec
	
def a.cpdown:
	shiftup sstos
	exec
	
def a.consup:
	notlst? sstos, .err_no_lst
	mov si, sstos
	unlst si
	List.link sstos, tos, si, .err_ll_failed
	lst sstos
	mov tos, sos
	down sos
	exec
   .err_no_lst: error "consup: list on upstack expected."
   .err_ll_failed: llerr "consup"
	
def a.unconsdown:
	notlst? sstos, .err_no_lst
	_ {cmp sstos, nil}, je .err_list_empty
	up sos
	mov sos, tos
	unlst sstos
	mov tos,   [sstos]
	mov sstos, [sstos + ws]
	lst sstos
	exec
    .err_no_lst: error "unconsdown: list on stack expected."
    .err_list_empty: error "unconsdown: list is empty."
	
def a.uzap:
	mov sstos, [ssp]
	sspPrev
	exec

def a.uinc:
	expectint sstos
	add sstos, one
	exec
;============================================================================
; :section:   diversa
;-----------------------------------------------------------------------------
def a.hiccup:
	mov si, [rsp]			; si := prog.calling
	unlst si
	_ {cmp si, zero}, jz .err_nil
	mov v,  [si]			; v := prog.calling[next-value]
	mov si, [si + ws]		; skip next-value in prog.calling
	shiftup v
	lst si
	mov [rsp], si			; write prog.calling back
	exec
    .err_nil: 		error "hiccup: instruction list empty."
    .err_list_expected:	error "hiccup: list as next instruction expected."

def a.return:
	clr prog
	exec

def a.nop:
	exec

def a.Stacks.clear:			; clear stack and upstack, not return-stack
	call Stacks.clear
	exec

def a.Stacks.print:
	callsave Stacks.print
	goto_exec

def a.Stacks.current:
	mov a, border_2
	sub a, dsp
	shr a, 3
	int a
	shiftup a
	mov a, border_3
	sub a, ssp
	shr a, 3
	int a
	shiftup a
	mov a, [rs_base]
	sub a, rsp
	shr a, 3
	int a
	shiftup a
	exec

def a.Stacks.reset:	; clear stack and upstack, reset return-stack, hiccup p to exec
	_ {cmp prog, zero}, jz .err_no_remaining_program
	mov v,  [prog]			; v := prog.calling[next-value]
	notlst? v, .err_no_list
	call Stacks.clear
	mov rsp, [rs_base]

	unlst v
	callfun f.execute, v
	jmp quit			; when this program ends

    .err_no_list:
	lf 
	strprint "############  Stacks.reset: no quotation found to restart with."
	lf 
	jmp exit_1

    .err_no_remaining_program:
	lf 
	strprint "############  Stacks.reset: no remaining program to restart with."
	lf 
	jmp exit_1

def a.Stacks.size:
	mov a, stackspace
	shr a, 3 
	int a
	shiftup a			; ds size
	shiftup a			; ss size
	call Syscall.return_stack_size
	int a
	shiftup a
	exec

def a.help:
def a.help_2:
	lf
	strprint ` ( )( )   Rabbit-vm    GPL 3.0    2018-January\n`
        strprint `          Running on x86_64 Linux.\n\n`
        strprint `rabbit takes whatever comes in it's commandline as program.\n`
        strprint `In order to prevent shell command substitution use '...'\n`
        strprint `So, at shell prompt type:\n`
        strprint `./rabbit 3 4 + iprint lf           - to add 3 and 4 and print the result.\n`
        strprint `./rabbit 3 4 '*()'cons lprint lf   - to cons the product of 3 and 4 to a list.\n`
        strprint `./rabbit '"Hallo world!"'print lf  - to say hallo.\n`
        strprint `./rabbit Symbol.list lprint        - for a list of inbuild atoms.\n\n`
	strprint `Rabbit-vm comes with no warrenty for nothing.\n`
	strprint `In case of difficulties restore data from backup.\n\n`
	goto_exec

	
def a.version:
def a.version_2:
	print0 mess_version
	lf
	goto_exec

def a.VM.version:
	callfun f.string0_to_list, mess_version
	shiftup a
	lst tos
	goto_exec

def a.VM.mem:
	mov a, bss_last
	sub a, bss_first
	int a
	shiftup a
	mov a, list_free
	sub a, list_mem
	int a
	shiftup a
	goto_exec

def a.VM.time:
	call Syscall.clock_time
	int a
	shiftup a				; sec
	int b
	shiftup b				; nanosec
	call Syscall.time_ue
	sub a, [rabbit_start_time]
	int a
	shiftup a				; VM uptime
	goto_exec

def a.exit:
	jmp exit
	
def a.exit_failure:
	jmp exit_1
	
def a.define:
	call Symtab.define
	exec
	
def a.nodef:
	mov k, tos
	unsym k
	strprint `\r\n############  not defined\r\n`
	xprint
	lf
	callsave Symtab.pos, k
	mov k, a
	strprint "file: "
	mov a, [k + o_pSrc]
	unlst a
	callsave f.list_print
	lf
	strprint "line: "
	mov a, [k + o_line]
	iprint a
	lf
	jmp Exec.vm_error

def a.nodef2:
	mov k, b					; a = fn
	mov si, b					; a = fn
	strprint `\r\n############  not defined (nodef2)\r\n`
	sym k
	callsave f.xprint, k
	lf
	callsave Symtab.pos, si
	mov k, a
	strprint "file: "
	mov a, [k + o_pSrc]
	unlst a
	callsave f.list_print
	lf
	strprint "line: "
	mov a, [k + o_line]
	iprint a
	lf
	jmp Exec.vm_error

def a.Time.ue:
	call Syscall.time_ue
	int a
	shiftup a
	exec

def a.Clock.msleep:
	notint? tos, .err_no_int
	shiftdown a
	uti a
	call Syscall.clock_msleep
	goto_exec
    .err_no_int: error "Clock.msleep: integer on stack expected."
	
error_print_list_expected: error "print: list on stack expected."
def a.print:
	notlst? tos, error_print_list_expected
	shiftdown a
	unlst a
	call f.list_print
    	exec

error_iprint_int_expected: error "iprint: integer on stack expected."
def a.iprint:
	notint? tos, error_iprint_int_expected
	shiftdown a
	uti a
	iprint a
	exec

error_lprint_list_expected: error "lprint: list on stack expected."
def a.lprint:
	lprint
	exec

def a.spc:
	spc
	goto_exec

def a.lf:
	lf
	goto_exec

def a.file_fetch:
	shiftdown k
	unlst k
	callfun  f.list_to_string0,   k
	callfun  Syscall.file_load,   string_buffer
	goto_exec

def a.file_store:
	shiftdown k
	unlst k
	callfun f.list_to_string0,   k
	callfun Syscall.file_store,  string_buffer
	goto_exec

def a.file_exists:
	unlst tos
	callsave f.list_to_string0, tos
	mov a, string_buffer
	call Syscall.file_is
	mov tos, a
	int tos
	goto_exec

def a.file_unlink:
	notlst? tos, .err_no_list
	unlst tos
	callsave f.list_to_string0, tos
	mov a, string_buffer
	call Syscall.file_unlink
	mov tos, a
	int tos
	goto_exec
    .err_no_list: error "File.unlink: list on stack expected."

def a.file_chmod:
	notint? tos, .err_no_int
	notlst? sos, .err_no_list
	unlst sos
	callsave f.list_to_string0, sos
	mov a, string_buffer
	mov b, tos
	uti b
	down sos
	call Syscall.file_chmod
	mov tos, a
	int tos
	goto_exec
    .err_no_list: error "File.chmod: list on stack expected."
    .err_no_int:  error "File.chmod: integer on stack expected."

def a.stdout_close:
	mov a, 0
	call Syscall.file_close
	mov a, 1
	call Syscall.file_close
	goto_exec

def a.parse_rabbit:
	callfun Parse.rabbit.pe, source_buffer, string_buffer
	_ {cmp a, 0}, jl .failed
    .success:
    	lst a
    	shiftup a
	exec
    .failed:
    	error "load: parsing failed."
	
;=============================================================================
; :section:   int math
;-----------------------------------------------------------------------------
def a.add:
	expectint tos, sos
	i.add
	exec

def a.sub:
	expectint tos, sos
	sub sos, tos
	mov tos, sos
	down sos
	exec

def a.mul:
	expectint tos, sos
	i.mul
	exec

def a.div:
	expectint tos, sos
	mov a, sos
	uti a
	uti tos
	cqo
	idiv tos 		; rex.w : 64 bit operation
	mov tos, a
	int tos
	down sos
	exec
	
def a.mod:
	expectint tos, sos
	mov a, sos
	uti a
	uti tos
	cqo
	idiv tos
	mov tos, d
	int tos
	down sos
	exec
	
def a.divmod:
	expectint tos, sos
	mov a, sos
	uti a
	uti tos
	cqo
	idiv tos
	mov tos, d
	int tos
	mov sos,a
	int sos
	exec

def a.muldivmod:
	expectint tos, sos
	down a
	uti a
	uti sos
	uti tos
	imul a, sos
	cqo
	idiv tos
	mov tos, d
	int tos
	mov sos,a
	int sos
	exec

def a.and:
	expectint tos, sos
	and tos, sos
	down sos
	exec
	
def a.or:
	expectint tos, sos
	or tos, sos
	down sos
	exec
	
def a.xor:
	expectint tos, sos
	xor tos, sos
	down sos
	exec
	
def a.shl:
	expectint tos, sos
	uti tos
	mov c, tos
	shl sos, cl
	mov tos, sos
	down sos
	exec
	
def a.shr:
	expectint tos, sos
	uti tos
	mov c, tos
	shr sos, cl
	mov tos, sos
	and tos, TYPE_UNMASK
	down sos
	exec
	
def a.asr:
	expectint tos, sos
	uti tos
	mov c, tos
	sar sos, cl
	mov tos, sos
	and tos, TYPE_UNMASK
	down sos
	exec

def a.abs:
	expectint tos
	mov a, tos
	neg a
	cmp tos, zero
	cmovl tos, a
	exec
	
def a.sgn:
	expectint tos
	mov a, -1
	int a
	mov c, one
	cmp tos, zero
	cmovl tos, a
	cmovg tos, c
	exec
	
def a.neg:
	expectint tos
	neg tos
	exec
	
def a.inv:
	expectint tos
	not tos
	and tos, TYPE_UNMASK
	exec
	
def a.shl1:
	expectint tos
	shl tos, 1
	exec
	
def a.shr1:
	expectint tos
	sar tos, 1
	and tos, TYPE_UNMASK		; clear lsb
	exec

def a.true:
	mov a, one
	shiftup a
	exec
	
def a.false:
	mov a, zero
	shiftup a
	exec

def a.not:
	mov a,   one
	cmp tos, 0
	mov tos, zero
	cmove tos, a
	exec
	
; -----------------------------------------------------------------------------
%macro comp_ 1
	xor a,a
	mov b, one
	cmp sos, tos
	%1 a, b
	mov tos, a
	down sos
    	exec
	%endmacro
	
def a.comp_g: 	comp_ cmovg
def a.comp_ge: 	comp_ cmovge
def a.comp_e: 	comp_ cmove
def a.comp_ne: 	comp_ cmovne
def a.comp_le: 	comp_ cmovle
def a.comp_l: 	comp_ cmovl

def a.comp_e_cs:
	mov a, one
	cmp sos, tos
	cmovne a, zero
	mov tos, a
	exec

%macro compz_ 1
	xor a,a
	mov b, one
	cmp tos, zero
	%1 a, b
	mov tos, a
    	exec
	%endmacro
	
def a.comp_ze: 	compz_ cmove

def a.inc:
	expectint tos
	add tos, one
	exec

def a.dec:
	expectint tos
	sub tos, one
	exec

;=============================================================================
; :section:   bool / tests
;-----------------------------------------------------------------------------
%macro typecheck_yes_no 0
    .yes:
    	mov tos, one
	exec
    .no:
    	mov tos, 0
	exec
	%endmacro
	
; --------------------------------------------------
def a.int?:
	notint? tos, .no
	typecheck_yes_no
	
def a.sym?:
	notsym? tos, .no
	typecheck_yes_no
	
def a.lst?:
	notlst? tos, .no
	typecheck_yes_no
	
; --------------------------------------------------
def a.int?cs:
	shiftup a
	notint? sos, .no
	typecheck_yes_no
	
def a.sym?cs:
	shiftup a
	notsym? sos, .no
	typecheck_yes_no
	
def a.lst?cs:
	shiftup a
	notlst? sos, .no
	typecheck_yes_no
	
; --------------------------------------------------
def a.type?:
	notsym? tos, .err_symbol_expected
	mov v, sos
	down sos
	notlst? v, .no
	unlst v
	_ {cmp v, zero}, jz .no
	mov v, [v]
	_ {cmp v, tos}, jz .yes
    .no:
    	mov tos, 0
	exec
    .yes:
    	mov tos, one
	exec
    .err_symbol_expected: error "type?: symbol on stack expected"
	
; --------------------------------------------------
def a.patt_cs: ; {l l -- l b}
	notlst? sos, .err_list_expected
	notlst? tos, .err_list_expected
	mov si, sos			; list of some values
	mov di, tos			; list of pattern
	unlst si
	unlst di

    .while: _ {cmp di, zero}, jz .lists_do_match
    	_     {cmp si, zero}, jz .lists_dont_match
	mov tos, [si]			; tos := some value to check
	mov si,  [si + ws]
	mov k,   [di]			; k := appropriate test
	mov di,  [di + ws]
	lst? k, .i_list			; test can be a quotation
	sym? k, .quote_symbol		; or a symbol (atom or defintion)
	jmp .err_sym_or_list		; but nothing else
	    
	    .quote_symbol:
		List.link k, k, zero, .err_ll_failed, si, di
		lst k

	    .i_list:
		_ lst si, lst di
		unlst k
		callfun f.execute, k, b,d,   si,di
		_ unlst si, unlst di
	_ {cmp tos, zero}, jz .lists_dont_match		; test passed?
    	jmp .while

    .lists_do_match:
    	mov tos, one
    	exec
	
    .lists_dont_match:
    	mov tos, zero
    	exec
	
    .err_list_expected: error "patt?*: list on stack expected."
    .err_sym_or_list: error "patt?*: only symbol or list as pattern element."
    .err_ll_failed:   llerr "patt?*"
;=============================================================================
; :section:   list
;-----------------------------------------------------------------------------
def a.first:
	notlst? tos, .err_list_expected
	_ {cmp tos, nil}, jz .err_nil
	unlst tos
	mov tos, [tos]
	exec
    .err_list_expected: error "first: list on stack expected."
    .err_nil: error "first: list is empty."
	
def a.first_cs:
	notlst? tos, .err_list_expected
	_ {cmp tos, nil}, jz .err_nil
	mov a, tos
	up sos
	unlst a
	mov sos, tos
	mov tos, [a]
	exec
    .err_list_expected: error "first*: list on stack expected."
    .err_nil: error "first*: list is empty."
	
def a.second:
	notlst? tos, .err_list_expected
	unlst tos
	_ {cmp tos, 0}, jz .err_nil
	mov tos, [tos + ws]
	_ {cmp tos, 0}, jz .err_nil
	mov tos, [tos]
	exec
    .err_list_expected: error "second: list on stack expected."
    .err_nil: error "second: empty_list"
	
def a.third:
	notlst? tos, .err_list_expected
	unlst tos
	_ {cmp tos, 0}, jz .err_nil
	mov tos, [tos + ws]
	_ {cmp tos, 0}, jz .err_nil
	mov tos, [tos + ws]
	_ {cmp tos, 0}, jz .err_nil
	mov tos, [tos]
	exec
    .err_list_expected: error "third: list on stack expected."
    .err_nil: error "third: empty_list"

; --------------------------------------------------
def a.solo:
	List.link tos, tos, zero, .err_ll_failed
	lst tos
	exec
    .err_ll_failed: llerr "solo"

def a.pair:
	List.link tos,  tos, zero,  .err_ll_failed
	List.link tos,  sos, tos,   .err_ll_failed
	lst tos
	down sos
	exec
    .err_ll_failed: llerr "pair"

def a.triple:
	List.link tos, tos, zero,  .err_ll_failed
	List.link tos, sos, tos,   .err_ll_failed
	down sos
	List.link tos, sos, tos,   .err_ll_failed
	lst tos
	down sos
	exec
    .err_ll_failed: llerr "triple"

def a.quad:
	List.link tos, tos, zero, .err_ll_failed
	List.link tos, sos, tos,  .err_ll_failed
	down sos
	List.link tos, sos, tos,  .err_ll_failed
	down sos
	List.link tos, sos, tos,  .err_ll_failed
	lst tos
	down sos
	exec
    .err_ll_failed: llerr "quad"

def a.unpair:
	notlst? tos, .err_no_lst
	unlst tos
	mov si, tos
	up sos
	_ {cmp si, zero}, je .err_no_pair
	mov sos, [si]
	mov si,  [si + ws]
	_ {cmp si, zero}, je .err_no_pair
	mov tos, [si]
	mov si,  [si + ws]
	_ {cmp si, zero}, jne .err_no_pair
	exec
    .err_no_lst: error "unpair: list on stack expected."
    .err_no_pair: error "unpair: pair on stack expected."

def a.untriple:
	notlst? tos, .err_no_lst
	unlst tos
	mov si, tos
	up sos
	_ {cmp si, zero}, je .err_no_pair
	mov k,   [si]
	mov si,  [si + ws]
	up k
	_ {cmp si, zero}, je .err_no_pair
	mov sos, [si]
	mov si,  [si + ws]
	_ {cmp si, zero}, je .err_no_pair
	mov tos, [si]
	mov si,  [si + ws]
	_ {cmp si, zero}, jne .err_no_pair
	exec
    .err_no_lst: error "untriple list on stack expected."
    .err_no_pair: error "untriple triple on stack expected."

def a.is_solo_cs:
	notlst? tos, err_no_lst_solo_cs
	up sos
	mov sos, tos
	jmp is_solo_checked
def a.is_solo:
	notlst? tos, err_no_lst_solo
is_solo_checked:
	mov si, tos
	unlst si
	mov tos, zero
	mov a, one
	cmp si, zero
	je .done
	mov si, [si + ws]
	cmp si, zero
	cmove tos, a
    .done:
	exec
err_no_lst_solo: error "solo?: list on stack expected."
err_no_lst_solo_cs: error "solo?*: list on stack expected."

def a.unlist:
	notlst? tos, .err_no_lst
	shiftdown si
	unlst si

    .while:
	_ {cmp si, zero}, je .done
	up sos
	mov sos, tos
	mov tos, [si]
	mov si, [si + ws]
	jmp .while
    .done:

	exec
    .err_no_lst: error "unlist: list on stack expected."

	
	
; --------------------------------------------------
def a.1drop:
	notlst? tos, .error_1drop_list_expected
	_ {cmp tos, nil}, jz .err_nil
	unlst tos
	mov tos, [tos + ws]
	lst tos
	exec
    .error_1drop_list_expected: error "1drop: list on stack expected."
    .err_nil: error "error_1drop_empty_list"

def a.elt:
	notlst? sos, .err_no_list
	notint? tos, .err_no_int
	unlst sos
	uti   tos
	mov si, sos
	mov c,  tos
	_ {cmp c, zero}, jl .err_neg_idx

	jmp .while
    .loop:
    	mov si, [si + ws]
	dec c
    .while:
    	_ {cmp c,  zero}, je .done
    	_ {cmp si, zero}, je .fail
	jmp .loop

    .done:
    	_ {cmp si, zero}, je .fail		; in case both are zero
	mov tos, [si]
	down sos
	exec
    .err_no_list: error "elt: list on stack expected."
    .err_no_int:  error "elt: int on stack exected."
    .err_neg_idx: error "elt: index is negative."
    .fail:	  error "elt: list too short."

def a.equal: ; {l l -- b}
	notlst? tos, .err
	notlst? sos, .err
	unlst tos
	unlst sos
	m4.equal tos, sos, tos
	down sos
	exec
    .err: error "equal?: list on stack expected."

def a.swons:
	xchg tos, sos
def a.cons:
	cons
	exec

def a.swonsd:
	shiftdown a
	push a
	xchg tos, sos
	cons
	pop a
	shiftup a
	exec

def a.conc:
	conc
	exec

def a.join:				
	notlst? sos, .err_no_lst
	mov si, sos
	unlst si
	xor c,c

	jmp .while_1
    .loop_1:				; push all elements of l to return-stack
    	mov a,  [si]
	mov si, [si + ws]
	inc c				; count elts
	push a
    .while_1: _ {cmp si, zero}, jne .loop_1
    
	List.link  si, tos, zero, .err_ll_failed	

    	jmp .while_2
    .loop_2:				; cons elts back from return-stack to new list
    
	pop d
	List.link si, d, si, .err_ll_failed
	
	dec c
    .while_2: _ {cmp c,zero}, jne .loop_2

    	mov tos, si			; return new lst
	lst tos
	down sos
	exec
    .err_no_lst: error "join: list on stack expected."
    .err_ll_failed: llerr "join"

def a.uncons:
	uncons
	exec

def a.unswons:
	uncons
	xchg tos, sos
	exec

def a.rev:
	rev
	exec

def a.copy:
	copy
	exec

def a.reverse:
	reverse
	exec

def a.swoncat:
	xchg tos, sos
def a.concat:
	concat
	exec

def a.append:
	append
	exec

def a.split:
	notlst? tos, .err_no_lst
	notlst? sos, .err_no_lst
	mov k, sos			; k := lst( list-to-split )
	mov v, tos			; v := lst( predicate quotation )
	mov si, zero			; si := true-l   p returned true
	mov di, zero			; di := false-l  p returned false
	down sos			; tos empty for each element of k to test

	
	jmp .while
    .loop:
	unlst k
	mov tos, [k]
	mov k,   [k + ws]
	lst k
	mov a, v
	unlst a
	push tos
	_ lst si, lst di
	_ push k, push v, push si, push di
	call f.execute
	_ pop di, pop si, pop v,  pop k
	_ unlst si, unlst di
	_ {cmp tos, zero}, jz .false
    .true:
	pop tos
	_ push k, push v
	List.link  si,  tos, si,  .err_ll_failed,  di
	_ pop v, pop k
    	jmp .while
    .false:
	pop tos
	_ push k, push v
	List.link  di,  tos, di,  .err_ll_failed,  si
	_ pop v, pop k
    .while: _ {cmp k, nil}, jnz .loop


	up sos
	mov sos, di
	mov tos, si
	lst tos
	lst sos
	rev
	xchg tos,sos
	rev
	exec
    .err_no_lst: error "split: list on stack expected."
    .err_ll_failed: llerr "split"

def a.nil:
	i.nil
	exec
	
def a.nild:
	i.nil
	xchg tos, sos
	exec
	
def a.is_nil:
	xor a,a
	mov b, one
	cmp tos, nil
	cmovnz tos, a
	cmovz  tos, b
	exec
	
def a.is_nilcs:
	xor a,a
	mov b, one
	mov d, tos
	cmp d, nil
	cmovnz d, a
	cmovz  d, b
	shiftup d
	exec
	
def a.is_nildcs:
	xor a,a
	mov b, one
	mov d, sos
	cmp d, nil
	cmovnz d, a
	cmovz  d, b
	shiftup d
	exec
	
;=============================================================================
; :section:   combinators
;-----------------------------------------------------------------------------
def a.wrd:
	mov a, [prog]
	mov prog, [prog + ws]
	shiftup a
	exec

def a.call:
	shiftdown a
	exec_x a
	exec
	
def a.i:
	notlst? tos, error_exec_only_lists
	mov a, tos
	mov tos, sos
	down sos
	unlst a
	call f.execute
	exec

def a.j:
	_ {cmp prog, zero}, je .err_last
	mov v,    [prog]
	mov prog, [prog + ws]
	notlst? v, .err_no_lst
	unlst v
	mov a, v
	call f.execute
	exec
   .err_last: error "j: not as last instruction in quotation"
   .err_no_lst: error "j: list as next instruction expected."

def a.if: ; ( bool  -- // prog: iftrue iffalse )
	shiftdown a
	_ {cmp a, zero}, jz .false	; a == zero  ->  bool == false   ->  false
    .true:
	mov v, [prog]
	mov prog, [prog + ws]
	mov prog, [prog + ws]
	mov a, v
	unlst a
	call f.execute
	exec
	
    .false:
    	mov prog, [prog + ws]
	mov v, [prog]
    	mov prog, [prog + ws]
	mov a, v
	unlst a
	call f.execute
	exec

def a.if_rl:
	shiftdown a
	_ {cmp a, zero}, jz .false
    .true:
    	mov prog, [prog]
	unlst prog
	exec
    .false:
    	mov prog, [prog + ws]
	mov prog, [prog]
	unlst prog
	exec

def a.ifnilcs_rl:
	_ {cmp tos, nil}, jnz .false
    .true:
    	mov prog, [prog]
	unlst prog
	exec
    .false:
    	mov prog, [prog + ws]
	mov prog, [prog]
	unlst prog
	exec

def a.ifnildcs_rl:
	_ {cmp sos, nil}, jnz .false
    .true:
    	mov prog, [prog]
	unlst prog
	exec
    .false:
    	mov prog, [prog + ws]
	mov prog, [prog]
	unlst prog
	exec

def a.iflezcs_rl:
	_ {cmp tos, zero}, jg .false
    .true:
    	mov prog, [prog]
	unlst prog
	exec
    .false:
    	mov prog, [prog + ws]
	mov prog, [prog]
	unlst prog
	exec

def a.ifl_rl:
	mov a, sos
	mov b, tos
	down tos
	down sos
	_ {cmp a, b}, jge .false
    .true:
    	mov prog, [prog]
	unlst prog
	exec
    .false:
    	mov prog, [prog + ws]
	mov prog, [prog]
	unlst prog
	exec

def a.iflcs_rl:
	mov a, sos
	mov b, tos
	mov tos, sos
	down sos
	_ {cmp a, b}, jge .false
    .true:
    	mov prog, [prog]
	unlst prog
	exec
    .false:
    	mov prog, [prog + ws]
	mov prog, [prog]
	unlst prog
	exec

def a.ife_rl:
	mov a, tos
	mov b, sos
	down tos
	down sos
	_ {cmp a, b}, jne .false
    .true:
    	mov prog, [prog]
	unlst prog
	exec
    .false:
    	mov prog, [prog + ws]
	mov prog, [prog]
	unlst prog
	exec

def a.unless:
	shiftdown k
	mov v,    [prog]
	mov prog, [prog + ws]
	_ {cmp k, zero}, jnz .false
    .true:
    	lst? v, .i_list
	sym? v, .quote_symbol
	jmp .err_only_list_or_symbol
	
    .quote_symbol:
	List.link k, v, zero, .err_ll_failed
	mov a, k
	call f.execute
	exec
    .i_list:
    	unlst v
	mov a, v
	call f.execute

    .false:
	exec
    .err_list_expected: error "unless: list on stack expected."
    .err_only_list_or_symbol: error "unless: list or symbol expected."
    .err_ll_failed: llerr "unless"

;=============================================================================
; :section:   Symbol
;-----------------------------------------------------------------------------
def a.symbol_sym:
	callfun Symtab.fn, tos
	sym a
	mov tos, a
	exec
	
def a.symbol_name:
	notsym? tos, .err_no_symbol
	unsym tos
	mov a, tos
	call Symtab.symbol
	mov tos, a
	exec
    .err_no_symbol: error "Symbol.name: symbol on stack expected."

def a.symbol_docstring:
	notsym? tos, .err_no_symbol
	unsym tos
	mov a, tos
	call Symtab.pos
	mov a, [a + o_docstring]
	mov tos, a
	exec
    .err_no_symbol: error "Symbol.docstring: symbol on stack expected."

def a.symbol_def:
	notsym? tos, .err_no_symbol
	unsym tos
	_ {cmp tos, last_atom_address}, jl .atom
    .lst:
	_ {cmp tos, zero}, jz .err_empty_fn
	
	; the position of the first element of def/fn/list must not move/change in list-space.
	; therefore: the fn returned by a.symbol_def gets a new allocated first elt,
	; otherwise, in case someone conses a new elt to the list returned from here we run
	; into difficulties.
	
	mov v,  [tos]
	mov si, [tos + ws]
	lst tos
	List.link  k,  v,si,  .err_ll_failed
	mov tos, k
	lst tos
	exec
    .atom:
    	sym tos				; for atoms: Symbol.def == symbol itself
	exec
    .err_no_symbol: error "Symbol.def: symbol on stack expected."
    .err_empty_fn:  error "something went wrong: Symbol.def: fn is empty (should have at least a nop)."
    .err_ll_failed: llerr "Symbol.def"

def a.symbol_source:
	notsym? tos, .err_no_symbol
	unsym tos
	mov a, tos
	call Symtab.pos
	mov k, [a + o_pSrc]
	mov v, [a + o_line]
	int v
	up sos
	mov sos, k
	mov tos, v
	exec
    .err_no_symbol: error "Symbol.source: symbol on stack expected."
    
def a.symbol_list:
	call Symtab.symbols
	lst a
	shiftup a
	exec
	
def a.is_atom:
	notsym? tos, .err_no_symbol
	unsym tos
	mov a, zero
	mov b, one
	cmp tos, last_atom_address
	cmovl tos, b
	cmovnl tos, a
	exec
    .err_no_symbol: error "atom?: symbol on stack expected."
	
;=============================================================================
; :section:   cmp			compare
;-----------------------------------------------------------------------------
;	compare like subtract get-sign:		a < b  ->  a - b  ->  -1
;						a = b  ->  a - b  ->   0
;						a > b  ->  a - b  ->   1
;	string/list: longer string is greater
;	1 2 3 a b c d dd ddd e dD A
;
;	int < sym < lst
;	123 < `ot < (1 2 3)
;	a list with different types sorted with cmp
;	gets first integers, second symbols and list at last.
;	( 4 a g 4 3 (a))  ->  (3 4 4 a g (a))
;
;-----------------------------------------------------------------------------
cmp_int:  ; ( int int -- int:-1|0|1 )
	cmp a, b
	mov c,  minus_one
	mov d,  one
	cmovl a, c			; cmp a,b:  a - b  <  0  ->  -1, c
	cmove a, zero			; cmp a,b:  a - b  == 0  ->   0, zero
	cmovg a, d			; cmp a,b:  a - b  >  0  ->   1, d
	ret

; -----------------------------------------------------------------------------
cmp_intlst: ; ( l l -- int:-1|0|1 )
	mov si, a
	mov di, b
	jmp .while
	
   .loop:
   	mov a,  [si]
	mov si, [si + ws]
	mov b,  [di]
	mov di, [di + ws]
	cmp a, b
	jl .return_minus_one
	jg .return_one
   .while:
   	_ {cmp si, zero}, je .si_ended
	_ {cmp di, zero}, je .return_one	; si longer than di  ->  si greater di  (a - b > 0)
	jmp .loop
	
    .si_ended:
    	_ {cmp di, zero}, je .return_zero	; both are nil
    .return_minus_one:				; si ended, di not  -> si shorter di  (a - b < 0)
	mov a, minus_one
	ret
	
    .return_zero:
    	mov a, zero
	ret
	
    .return_one:
	mov a, one
	ret

; -----------------------------------------------------------------------------
cmp_lst: ; ( l l -- int:-1|0|1 )
	mov si, a
	mov di, b
	jmp .while
	
   .loop:
   	mov a,  [si]
	mov si, [si + ws]
	mov b,  [di]
	mov di, [di + ws]
	callsave f.compare, a
	cmp a, zero
	jl .return_minus_one
	jg .return_one
   .while:
   	_ {cmp si, zero}, je .si_ended
	_ {cmp di, zero}, je .return_one	; si longer than di  ->  si greater di  (a - b > 0)
	jmp .loop
	
    .si_ended:
    	_ {cmp di, zero}, je .return_zero	; both are nil
    .return_minus_one:				; si ended, di not  -> si shorter di  (a - b < 0)
	mov a, minus_one
	ret
	
    .return_zero:
    	mov a, zero
	ret
	
    .return_one:
	mov a, one
	ret
; -----------------------------------------------------------------------------
cmp_sym: ; ( sym sym -- int:-1|0|1 )
	mov k, a
	mov v, b
	unsym k
	unsym v
	callsave Symtab.symbol, k
	mov k, a
	callsave Symtab.symbol, v
	mov v, a
	unlst k
	unlst v
	callsave cmp_intlst, k,v
	ret
	
; -----------------------------------------------------------------------------
f.compare: ; ( x y -- int:-1|0|1 )	
	mov k, a
	mov v, b
	int? v, .cmp_int
	sym? v, .cmp_sym
	lst? v, .cmp_lst
	jmp .err_something_wrong
	
    .cmp_int:
    	sym? k, .return_one
	lst? k, .return_one
	mov a, k
	mov b, v
	call cmp_int
	ret

    .cmp_sym:
    	int? k, .return_minus_one
	lst? k, .return_one
	mov a, k
	mov b, v
	call cmp_sym
	ret

    .cmp_lst:
    	int? k, .return_minus_one
	sym? k, .return_minus_one
	unlst k
	unlst v
	callsave cmp_lst, k,v
	ret

    .return_one:
    	mov a, one
	ret
    .return_minus_one:
    	mov a, minus_one
	ret

    .err_something_wrong: error "cmp: something went wrong"
    .err_types_differ: error "cmp: types differ"
	ret
	
; --------------------------------------------------
roadwork_cmp: error "cmp: ROADWORK"

def a.cmp:
	callfun f.compare,  sos,tos
	mov tos, a
	down sos
	exec

def a.within_cs:
	mov  b, sos
	down sos
	mov  d, tos
	mov  a, sos
	jmp within
	
def a.within:
	down a			; a := i   b := j   d := k   (j <= i < k)   (b <= a < d)
	mov  b, sos
	mov  d, tos
	down sos

within:
	sub  d, one
	sub a, b
	sub d, b
	cmp d, a
	mov   tos, one
	cmovb tos, zero		; mov if below:  d < a   --> false
	exec

	;=============================================================================
def a.xterm_size:
	call Syscall.xterm_size
	up sos
	up tos
	mov sos, a
	mov tos, b
	int tos
	int sos
	goto_exec
;=============================================================================
def a.list_gc: ; ( -- i i  // runs free )
	callfun List.gc
	up sos
	up tos
	mov sos, a
	mov tos, b
	int sos
	int tos
	exec
;=============================================================================
def a.readchr: ; ( -- c  // read char from stdin )
	call Syscall.cread
	shiftup a
	int tos
	exec
	
;=============================================================================
; :section:   Stringbuffer
;-----------------------------------------------------------------------------
def a.stribu_print: ; ( -- // print current contents of stringbuffer )
	call Stribu.print0
	goto_exec

def a.stribu_clear: ; ( -- // clear contents of stringbuffer )
	call Stribu.clear
	exec

def a.stribu_append_char: ; ( c -- // append char to stringbuffer )
	shiftdown a
	uti a
	call Stribu.append_char
;	call Stribu.close
	exec

def a.stribu_iscan: ; ( -- i // read i from stringbuffer )
	mov a, string_buffer
	mov b, [string_buffer_current]
	add b, string_buffer
	call f.iscan
	shiftup a
	int tos
	exec
	
;=============================================================================
; :section:   Sourcebuffer
;-----------------------------------------------------------------------------
def a.srcbu_print: ; ( -- // print current sourcebuffer )
	call Sourcebuffer.print
	goto_exec
	
def a.srcbu_append: ; ( lc --  // append chars from list l to current contents of sourcebuffer )
	call Sourcebuffer.append
	exec
    
def a.srcbu_content: ; ( -- lc  // return content of Sourcebuffer as char-list )
	call Sourcebuffer.content
	lst a
	shiftup a
	exec
    
;=============================================================================
; :section:   System
;-----------------------------------------------------------------------------
def a.Sys_fork:
	call Syscall.sys_fork
	int a
	shiftup a
	goto_exec

def a.Sys_execve:  ; ( s:file-name sl:args sl:env  -- )
	down k					; k := file-name
	notlst? k,   .err_no_list
	notlst? sos, .err_no_list		; sos := args
	notlst? tos, .err_no_list		; tos := env
	nil?    sos, .err_no_filename
	unlst k
	unlst sos
	unlst tos
	callsave f.list_to_string0, k		; write filename to stringbuffer

	mov k, buffer				; k := *args[]  == buffer
	clr v					; v := *env[]
	mov di, k				; di := index in args[], later index in env[]

	jmp .while_args
    .loop_args:
	call Stribu.current
	add a, string_buffer
	mov [di], a				; [di] := first free of stringbuffer
	add di, ws
    	mov d,   [sos]				; d := arg
	mov sos, [sos + ws]
	notlst? d, .err_no_list_arg
	unlst d
	callsave f.list_to_string0_appending, d	; append arg to stringbuffer
    .while_args: notzero? sos, .loop_args

    	mov [di], zero				; terminate args with NULL
	add di, ws
	mov v, di				; v := *env[]

    	jmp .while_env
    .loop_env:
	call Stribu.current
	add a, string_buffer
	mov [di], a				; [di] := first free of stringbuffer
	add di, ws
    	mov d,   [tos]				; d := env
	mov tos, [tos + ws]
	notlst? d, .err_no_list_env
	unlst d
	callsave f.list_to_string0_appending, d	; append env to stringbuffer
    .while_env: notzero? tos, .loop_env

    	mov [di], zero				; terminate env with NULL

	down tos
	down sos

	mov a, string_buffer			; a := *str0:file-name
	mov b, k				; b := *args[]
	mov d, v				; d := *env[]
	call Syscall.sys_execve   		; a := *str0:filename  b := *argv[]  d := *env[]
	error "Sys.execve returned."
    .err_no_list:     error "Sys.execve: list on stack expected."
    .err_no_filename: error "Sys.execve: filenname as first arg of args expected."
    .err_no_list_arg: error "Sys.execve: string as arg expected."
    .err_no_list_env: error "Sys.execve: string as env expected."

def a.Sys_pipe2:
	notint? tos, .err_no_int
	shiftdown a
	uti a
	call Syscall.pipe2
	int b
	shiftup b
	int d
	shiftup d
	int a
	shiftup a
	goto_exec
    .err_no_int: error "Sys.pipe2: int on stack expected."

def a.Sys_dup2:
	notint? tos, .err_no_int
	notint? sos, .err_no_int
	shiftdown a
	uti a					; old fd
	shiftdown b
	uti b					; new fd
	call Syscall.dup2
	int a
	shiftup a				; new fd
	goto_exec
    .err_no_int: error "Sys.dup2: integer on stack expected."

def a.Sys_fd_close:
	notint? tos, .err_no_int
	shiftdown a
	uti a
	call Syscall.fd_close
	goto_exec
    .err_no_int: error "Sys.fd-close: integer on stack expected."
    
def a.Sys_fd_read:
	notint? tos, .err_no_int
	shiftdown a
	uti a
	call Syscall.file_read
	mov [source_buffer_current], b
	goto_exec
    .err_no_int: error "Sys.fd-read: integer on stack expected."
    
def a.Sys_fd_write:
	notint? tos, .err_no_int
	shiftdown a
	uti a

	mov k, a
	call Sourcebuffer.current
	dec a					; without trailing zero
	mov d, a
	_ {cmp d, source_buffer_size}, jge .err_strange_length
	syscall_4 SYS64_WRITE, k, source_buffer, d
	_ {cmp a, zero}, jl .err_store

	goto_exec
    .err_no_int: error "Sys.fd-write: integer on stack expected."
    .err_strange_length: error "Sys.fd-write: strange length."
    .err_store: error "Sys.fd-write: failed."
    
;=============================================================================
def a.Sys_socket:
	down k
	notint? k,   .err_no_int			; family
	notint? sos, .err_no_int			; type
	notint? tos, .err_no_int			; protocol
	uti k
	mov a, k
	mov b, sos
	uti b
	mov d, tos
	uti d
	shiftdown sos
	
	call Syscall.socket
	mov tos, a
	int tos
	goto_exec
    .err_no_int: error "Sys.socket: integer on stack expected."

def a.Sys_connect:
	notint? tos, .err_no_int
	notint? sos, .err_no_int
	mov a, sos
	uti a
	mov b, tos
	uti b
	call Syscall.connect
	mov tos, a
	int tos
	shiftdown sos
	goto_exec
    .err_no_int: error "Sys.connect: integer on stack expected."

def a.Sys_waitid:
	notint? tos, .err_no_int
	mov a, tos
	uti a
	call Syscall.waitid
	mov tos, a
	int tos
	goto_exec
    .err_no_int: error "Sys.waitid: integer on stack expected."
;=============================================================================
last_atom_address:
;=============================================================================
error_nlb_not_wall: llerr "List.next: new list box value is not WALL."
error_nlb_not_wall_from_link: llerr "List.link: new list box value is not WALL."

err_ll_failed_conc: llerr "conc"
err_ll_failed_cons: llerr "cons"
err_ll_failed_concat: llerr "concat"
err_ll_failed_reverse: llerr "reverse"
err_ll_failed_copy: llerr "copy"


error_int_expected: 	error "Integer on stack expected."
error_list_expected: 	error "List on stack expected."
err_list_is_nil:    	error "copy,conc: List is nil"
error_uncons_nil: 	error "uncons: list is empty"

;=============================================================================
