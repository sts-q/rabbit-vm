; -*- mode: mynasm;-*-
;=============================================================================

; :file:   rabbit_lib.asm		utility functions	
;=============================================================================
;   Rabbit-vm   An approach to the Joy Programming Language.
;   GNU GENERAL PUBLIC LICENSE, Version 3.0, http://www.gnu.org/licenses/
;   https://bitbucket.org/sts-q/rabbit-vm
;   Copyright (C) 2018 Heiko Kuhrt
;   Heiko.Kuhrt ( at ) yahoo.de
;=============================================================================
; :chapter:   lib			utility functions
section .text
; -----------------------------------------------------------------------------
f.iprint: ; ( i -- ) print integer				; a f
	xor   k,	k			; set sign flag in k
	cmp   a, 	0
	jns .begin
	mov   k,  	1
	neg   a

    .begin:					
    	mov   b, 	buffer_last		; buffer-pointer: b
	mov   [b],	byte 0			; set end of string: 0
	mov   c,  	10			; a mod 10: modulo: d, remainder: a

    .loop:					; prepend digits until (remainder == 0)
	cqo					; sign extend rax into rdx
	div   c
	add   d,  	48			; i -> digit
	dec   b
	mov   [b],  	dl
	cmp   a,     	0
	jnz .loop

	cmp   k,   	0			; prepend sign
	jz .fin
	dec   b
	mov   [b],	byte '-'			

    .fin:
    	mov a, b				; print
	call Syscall.print0

	ret	

; -----------------------------------------------------------------------------
f.iscan: ; ( startaddr endaddr+1 -- int  // 0 -0 +0 1 -1 +1 -123 +123 123  -> integer, no more checks)
	mov si, a			; from si
	mov di, b			; exclusive di (endaddr + 1), first non-integer-char
	mov a, 0			; result
	xor b,b				; digit
	mov v, 1			; sign

	mov bl, byte [si]		; skip optional +
	cmp bl, '+'
	jz .skip_sign
	cmp bl, '-'			; skip optional -, reverse sign
	jnz .loop
	mov v, -1
    .skip_sign:
    	inc si
    .loop:
	imul a, 10
    	mov bl, byte [si]
	sub b, '0'
	add a, b
	inc si
	cmp si, di
	jl .loop

	imul a, v
	ret
	
;=============================================================================
; :section:   Stringbuffer
;-----------------------------------------------------------------------------
Stribu.clear:	;						a
	clr a
	mov [string_buffer_current], a	; current := 0  ->  0 is first empty position
	mov [string_buffer],         a	; write terminating zero(s)
	ret

Stribu.current: ; ( -- i  // current first empty position in buffer )  a
	mov a, [string_buffer_current]
	ret

Stribu.append_char: ; ( chr --  // append char to string-buffer, write new terminating zero ) a
	mov b, [string_buffer_current]
	mov [string_buffer + b], byte al
	inc b
	mov [string_buffer + b], byte 0
	_ {cmp b, string_buffer_size}, jge error_string_buffer_overflow
	mov [string_buffer_current], b
	ret

Stribu.close: ; ( -- i  // return current number of chars in buffer, write closing zero to buffer )
	mov a, [string_buffer_current]
	mov [string_buffer + a], byte 0
	inc a
	mov [string_buffer_current], a
	ret					; return a

Stribu.print0:							; a
	print0 string_buffer
	ret

error_string_buffer_overflow: error "Stribu.append_char: string_buffer_overflow"

;=============================================================================
; :section:   commandline
;-----------------------------------------------------------------------------
collect_commandline: ; ( dest --  // write commandline into dest (sourcebuffer) )   a f
	mov c, [argc]
	mov k, [argv]
	dec c				; skip program-name
	add k, 8
	mov di, source_buffer


   .while: _ {cmp c, zero}, jz .done
   	add k, 8			; k := **current-arg
	dec c
	mov si, [k]			; si := *current-arg
	xor a,a
	mov al, [si]			; a := current-char

    .while_2: _ {cmp a, zero}, jz .done_2
        mov [di], al			; srcbu[di] := chr
	inc di
	inc si
	_ {cmp di, source_buffer_last}, jge .buffer_overflow
	xor a,a
	mov al, [si]
        jmp .while_2
	
    .done_2:
	mov al, 32
	mov [di], al
	inc di
	jmp .while

	
    .done:
	mov [di], zero
	ret

    .buffer_overflow: error "commandline too long."


;=============================================================================
