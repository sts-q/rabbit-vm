; -*- mode: mynasm;-*-
;=============================================================================

; :file:   rabbit_gc.asm		garbage collector for list elements

;=============================================================================
;   Rabbit-vm   An approach to the Joy Programming Language.
;   GNU GENERAL PUBLIC LICENSE, Version 3.0, http://www.gnu.org/licenses/
;   https://bitbucket.org/sts-q/rabbit-vm
;   Copyright (C) 2018 Heiko Kuhrt
;   Heiko.Kuhrt ( at ) yahoo.de
;=============================================================================
; not gc root:
;	si di   k v c   a b d	; tagged as lst and push to rs if points to useful list
;
; gc root:		collect only	could also be
;	tos sos		lst 		int sym
;	sstos		lst 		int sym
;	ds ss 		lst 		int sym
;   	rs		lst		anyting
;	prog		l   
;	Symtab:
;		fn	l  		atom-addr
;		name    lst
;	   docstring	lst
;		src	lst
;
; Rabbit-vm currently shows three data types to the user: lst int sym.
; Intern there are 7 data types: lst/int/sym tagged or untagged plus addresses:
;	lst l int i sym s addr
;
; Values tagged as lst get collected.
;
; Possible checks for integrity:
;	lst
;		points into lst-space?		list-space:  list_mem <= X < list_free
;		is 16-byte aligned?
;
;	sym
;		points into sym-space?		symbol-space:	first_atom_address <= X < list_free
;		is 8-byte aligned?
;
;	int
;		hmm?
section .text

;============================================================================
; :section:   lib gc
;-----------------------------------------------------------------------------
%macro gc_start_info 0
	  lf
	  strprint "#######   List.gc ... "
	  %endmacro

%macro gc_done_info 0  ; ( runs free -- )
	push a
	push b
	  strprint "< "
	  mov a, [rsp + ws]			; runs
	  iprint_ a
	  mov a, maximal_list_elements		; used
	  sub a, [rsp]
	  iprint_ a
	  mov a, maximal_list_elements		; max
	  iprint a
	  strprint " > "
	  cprint 13
	pop b
	pop a
	  %endmacro


%macro isnot_l? 2 ; ( l dest )
	; some x on rs may look like lst but are not:
	; untagged int and addr
	mov a, %1
	unlst a
	_ {cmp a, list_mem},	jl  %2
	_ {cmp a, list_free},	jge gc_err_isnot_l		; yes
	_ {test a, 15}, 	jnz %2
	%endmacro


gc_err_isnot_l:
	_ lf, strprint "GC failed: lst isnot_l in rs", lf, jmp exit_1

	
;============================================================================
; :section:   mark
;-----------------------------------------------------------------------------
;	if k is a list
;		with each list element
;			break if element has been marked
;			set gc-bit of link
;			recurse on value
;-----------------------------------------------------------------------------
%macro mark 1  ;  ( item -- )
	mov c, %1
	notlst? c,         %%done	; no lst  ->  no gc
	_ {cmp c, nil}, je %%done	; nil in tos,sos,ds etc  ->  don't mark (but pls mark nil in list-space)
    %%mark:
	unlst c
	callsave f.mark, c,b		; here is where recur happens  ->  callsave
    %%done:
	%endmacro

;-----------------------------------------------------------------------------
f.mark: ; ( l -- ) ; l is never nil
	mov si, a			; si := l

    .loop:
    	mov v,  [si]			; v  := value
	mov di, [si + ws]		; di := next
	bt  di, GC_BIT
	jc .done
	
	mark v				; recur on value

	mov d, di			; write link with gc_bit set, mark nil, too
	bts d, GC_BIT			
	mov [si + ws], d

	mov si, di
    .while: _ {cmp si, zero}, jne .loop
    
    .done:
	ret

;============================================================================
; :section:   sweep
;-----------------------------------------------------------------------------
;	loop over all list elements, from bottom to top one after one
;		if marked then
;			unmark
;		else
;			link to last not marked
;-----------------------------------------------------------------------------
sweep:
	mov si, list_mem		; si := start of list-mem-space
	mov di, zero			; di := new list of free elements
	mov v, list_free		; v := list-end   (==  pos of var list_free)
	sub v, (ws * 2)			; v := last list-elment position
	xor c,c				; c := 0
	mov k, WALL
	
	jmp .while_2
    .loop:
    	mov a, [si + ws]		; a := link
	bt  a, GC_BIT
	jnc .link			; if not marked then link

      .unmark:				; if marked then unmark
    	btr a, GC_BIT
	mov [si + ws], a		; link := unmarked( link )
;    check_list_element [si], [si + ws], gc_err_ll_failed	; uses a b d
	add si, (ws * 2)
      .while: _ {cmp si, v}, jle .loop
	jmp .done
	
      .link:				; link to new list of free elements
	mov [si],      k		; clear old v with WALL
    	mov [si + ws], di
	mov di, si
	add si, (ws * 2)
    	inc c
      .while_2: _ {cmp si, v}, jle .loop

    .done:
    	mov [list_free], di
	_ {cmp c, zero}, je gc_err_no_space_left
	mov a, c			; return length of new free list
	ret

gc_err_no_space_left:
    	_ lf, strprint "GC failed: no space left.", lf
   	jmp exit_1

gc_err_ll_failed:
	_ lf, strprint "GC failed: check list element failed.", lf
	jmp exit_1
	
;============================================================================
; :section:   mark-stacks
;-----------------------------------------------------------------------------
%macro mark_data_stack 0
	mov si, dsp				
	mov di, border_2			

	jmp .while_ds
    .loop_ds:
    	mark [si]
	add si, ws
    .while_ds: _ {cmp si, di}, jl .loop_ds

    	%endmacro


%macro mark_software_stack 0
	mov si, ssp				
	mov di, border_3

	jmp .while_ss
    .loop_ss:
    	mark [si]
	add si, ws
    .while_ss: _ {cmp si, di}, jl .loop_ss

    	%endmacro


%macro mark_return_stack 0
	mov si, rsp				; points full
	mov di, [rs_base]			
	
	jmp .while_rs
    .loop_rs:
      	mov k, [si]
	notlst?  k, .mark_rsx_done		
	isnot_l? k, .mark_rsx_done
    	mark k
      .mark_rsx_done:
	add si, ws
    .while_rs: _ {cmp si, di}, jl .loop_rs	; first push incs from rs_base + call f.execute 

    	%endmacro

;============================================================================
; :section:   mark-symtab
;-----------------------------------------------------------------------------
%macro mark_symtab 0
	mov si, symtab				; mark symtab: fn, name, docstring, src
	mov di, [symtab_ff]
	jmp .while_symtab
	
      .loop_symtab:
	mov k, [si]				; k := fn
	unsym k
	_ {cmp k, last_atom_address}, jl .def_done	; don't mark atoms
	lst k
	mark k
      .def_done:
	mark [si + o_name]
	mark [si + o_docstring]
	mark [si + o_pSrc]
	add si, tFunc_size
    .while_symtab: _ {cmp si, di}, jl .loop_symtab
    
    	%endmacro
	
;============================================================================
; :section:   gc			mark and sweep
;-----------------------------------------------------------------------------

List.gc:  ;  ( -- runs free )   call with: 'callfun List.gc'    (callee saves)

;	gc_start_info

	mov [List.gc.temp_si], si
	mov [List.gc.temp_di], di
	mov [List.gc.temp_k],  k
	mov [List.gc.temp_v],  v
	mov [List.gc.temp_c],  c
	mov [List.gc.temp_d],  d

	mark tos	
	mark sos	
	mark sstos
	lst prog
	mark prog
	unlst prog

	mark_data_stack
	mark_software_stack
	mark_return_stack

	mark_symtab

	mov k, [CurrentSource]
	lst k
	mark k
    
	call sweep			; sweep ( -- free )

	mov b, c			; return free in b
	mov a, [List.gc.runs]		
	inc a
	mov [List.gc.runs], a		; return runs in a

	mov si, [List.gc.temp_si]
	mov di, [List.gc.temp_di]
	mov k,  [List.gc.temp_k]
	mov v,  [List.gc.temp_v]
	mov c,  [List.gc.temp_c]
	mov d,  [List.gc.temp_d]

%if GC_CHECKS == on
	gc_done_info
%endif
	
	ret

;=============================================================================
