; -*- mode: mynasm;-*-
;=============================================================================

; :file:   rabbit_prelib.asm		utility macros

;=============================================================================
;   Rabbit-vm   An approach to the Joy Programming Language.
;   GNU GENERAL PUBLIC LICENSE, Version 3.0, http://www.gnu.org/licenses/
;   https://bitbucket.org/sts-q/rabbit-vm
;   Copyright (C) 2018 Heiko Kuhrt
;   Heiko.Kuhrt ( at ) yahoo.de
;=============================================================================
; :section:   print
;-----------------------------------------------------------------------------
%macro cprint 1  ; ( %1:i:chr -- )  print 1 chr
	callfun Syscall.cprint, %1
	%endmacro

%macro lf 0  ; ( -- )  print lf
	cprint 10
;	cprint 13
	%endmacro

%macro spc 0  ; ( -- ) print spc
	cprint 32
	%endmacro
	
%macro print0 1 ; ( %1:addr:str0 -- )
	callfun Syscall.print0, %1
	%endmacro

%macro strprint 1
	section .data
	gensym_next
	gensym : db %1 ,0
	section .text
	print0 gensym
	%endmacro

%macro xx 0
	strprint `\r\n\n############ xx \r\n`
	call Stacks.print
	jmp exit_1
	%endmacro

%macro error 1-3  ; ( str0const  str0addr -- )			a f
	%if %0 > 1
	    mov k, %2
	    %endif
	%if %0 = 3
	    mov v, %3
	    %endif
	print0 mess_error
	strprint %1
	%if %0 > 1
	    print0 k
	    %endif
	%if %0 = 3
	    lf
	    iprint v
	    %endif
	lf
	jmp Exec.vm_error
	%endmacro

%macro  iprint 1
	callsave f.iprint, %1
	%endmacro

%macro iprint_ 1
	iprint %1
	spc
	%endmacro


;=============================================================================
%macro gc_now 0
	_ push a, push b, push d
	print0 mess_gc_now_short
	mov [list_free], zero
	_ pop d, pop b, pop a
	%endmacro

;=============================================================================



