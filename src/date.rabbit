;=============================================================================

; :file:   date.rabbit                  date calculation: 1.1.1900 to 31.12.2100

;==============================================================================
; :chapter:section:   Ida
;=============================================================================
(
module Ida
        index-of-1.1.1970 {} 25567

        -unlist {date -- dd mm yyyy} unlist
        leapyears {year -- i} dec Date._.vy/f - 4 /             ; dec: don't count running year
        
        full-month-days {mm year -- mm days}
                Date._.mlens swap  
                dec take sum                            ; dec: don't add days of running month
        full-year-days {mm year -- days} ( Date._.vy/f -  365 * )( leapyears )-bi   +

;--------------------------------------------------
        -year {i -- y} inc 100 * 36525 /   Date._.vy/f +        ; inc: rounding hits the target better
        rest-days {y i -- y i i} over (1 1)swap join Ida.<-date   -   1 +
        
        step-down {mm restdays -- mm dd  // up: mlens}
                | unconsdown 2dup  >    |  - ^inc rec
                others                  | down 2zap
        mmdd {y i --y i i // yyyy mm dd}
                (dup Date._.mlens up)dip
                1 swap
                step-down


;-----------------------------------------------------------------------------
interface

<-date {date -- ida}
                        -unlist ( full-month-days )( full-year-days )-bi + +
                        1 -    ; 1 1 1900 -> 0, not 1

->date {ida -- date}    dup ^-year  rest-days   mmdd  rotate triple


->t {ida -- t}          index-of-1.1.1970 -   86400 *
<-t {t   -- ida}        86400 /   index-of-1.1.1970 +


;-----------------------------------------------------------------------------
documentation
__info {} "Conversion of time and date to and from ida: index of day since 1.1.1900
ida:  i index of day        0 ->  1. 1.1900  vy/f  valid year first
                        25567 ->  1. 1.1970  ue    unix epoch begin
                        73413 -> 31.12.2100  vy/l  valid year last"
                        
end ; Ida
;==============================================================================
; :chapter:section:   Date
;==============================================================================
module Date

vy/f {-- i  // vaild-year/first: first year where this should work for}   1900
vy/l {-- i  // valid-year/last:  last year, where this should work for}   2100

days {} (
                (1   "Sonntag"      )
                (2   "Montag"       )
                (3   "Dienstag"     )
                (4   "Mittwoch"     )
                (5   "Donnerstag"   )
                (6   "Freitag"      )
                (7   "Samstag"      ))

months {} (
                ( 1   "Januar"    "Jan"   "01")
                ( 2   "Februar"   "Feb"   "02")
                ( 3   "Maerz"      "Mrz"   "03")
                ( 4   "April"     "Apr"   "04")
                ( 5   "Mai"       "Mai"   "05")
                ( 6   "Juni"      "Jun"   "06")
                ( 7   "Juli"      "Jul"   "07")
                ( 8   "August"    "Aug"   "08")
                ( 9   "September" "Sep"   "09")
                (10   "Oktober"   "Okt"   "10")
                (11   "November"  "Nov"   "11")
                (12   "Dezember"  "Dez"   "12"))
                
        no  {-- l  // lengths of months}               (31 28 31  30 31 30   31 31 30  31 30 31)
        yes {-- l  // lengths of months in a leapyear} (31 29 31  30 31 30   31 31 30  31 30 31)
mlens {year -- l  // list with lengths of months of year}   Date.leapyear? if(yes)(no)


;==============================================================================
interface

leapyear? {year -- b}
                | dup 400 mod  0 =      | zap true
                | dup 100 mod  0 =      | zap false
                | dup   4 mod  0 =      | zap true
                others                  | zap false
                
month-length {year month -- i}   ( mlens )( dec )bi elt  

valid? {date -- b}
                unlist
                2dup swap month-length up
                | dup  2100 >           | 3zap false  down zap
                | 1900 swap >           | 2zap false  down zap
                | dup 12 >              | 2zap false  down zap
                | 1 swap >              |  zap false  down zap
                | dup down >            |  zap false
                | 1 swap >              |      false
                others                  | true

wd {ida -- weekday  // (1..7) -> (so..sa)}   1 +   7 mod   inc
;weekday-matches? {date s -- b} ( weekday )( weekday-read )bi = 
weekday   {i -- weekday}        ^days assoc second
monthname {i -- monthname}      ^months assoc second

parse {s -- date  // 1.2.2012 -> ( 1 2 2012) }    "."chunks  ( stoi )map   

show {date -- s}   ( Int.show-00 )map  ( "."swoncat concat )fold1

current {-- date}   Time.ue Ida.<-t Ida.->date
today{-- s}   current show
ifmt {-- s}   current   ( rotate )infra   ( Int.show-00 )map   Chr.- enconcat



;==============================================================================
documentation

__info {} "Functions about dates.   (dd mm yyyy)
type    day month year  i
        ida             i                       index of day, module Ida
        date            (day month year)
        wd              i                       1..7
        monthname       s                       'Januar' 'Februar' ...
        weekday         s                       'Sonntag'  'Montag' ...
        "
        
__man {}  __info Dot.brown print lf
        "This module should work from/to: "print
        vy/f vy/l   ((1 1)swap join show )bi-   ^print spc print lf

;-----------------------------------------------------------------------------
)Define

;=============================================================================
