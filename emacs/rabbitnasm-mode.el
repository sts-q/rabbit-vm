;;=============================================================================
;;
;; :file:   mynasm-mode.el		emacs syntax highlighting for rabbits NASM sources
;;
;;=============================================================================
;;
;; README
;;
;;
;;
;; CONTACT
;;
;; https:/bitbucket.org/sts-q
;;
;;
;; LICENCE
;;
;; GPL 3.0
;;
;;
;;     -*- mode: mynasm -*-
;;
;;=============================================================================
;; :section:   mynasm-font-lock-function
;;-----------------------------------------------------------------------------

(defun mynasm-font-lock-function()
  "return a ( regexp . color ) list"
  (let* ((head   "\\<")                
	 (tail   "\\>")
	 (word   "[[:word:]]+")
	 (stack-comment   "( [^)]* )")
	 (faces 
	  `( 
	    ;; ---------------------------------------------------------
	    (,(concat "/\\*" ".*" "\\*/") . font-lock-comment-face)
	    ;; ---------------------------------------------------------
	    ;; one line macro
	     (,(concat "\\(" head "%define" tail "\\)\\s-*\\(" word "\\)")         ; {[^}]*}
	      (1  font-lock-keyword-face)
	      (2  font-lock-function-name-face))

	     (,(concat "\\(" head "def" tail "\\)\\s-*\\(" word "\\)")         ; {[^}]*}
	      (1  font-lock-keyword-face)
	      (2  font-lock-function-name-face))

	     (,(concat "\\(" head "%assign" tail "\\)\\s-*\\(" word "\\)")         ; {[^}]*}
	      (1  font-lock-keyword-face)
	      (2  font-lock-function-name-face))

	    ;; multi line macro
	     (,(concat "\\(" head "%macro" tail "\\)\\s-*\\(" word "\\)")         ; {[^}]*}
	      (1  font-lock-keyword-face)
	      (2  font-lock-function-name-face))

	    ;; multi line macro
	     (,(concat "\\(" head "struc" tail "\\)\\s-*\\(" word "\\)")         ; {[^}]*}
	      (1  font-lock-keyword-face)
	      (2  font-lock-function-name-face))

	    ;; label
	    (,(concat word "\\:")  .  font-lock-variable-name-face)
	    
	    
	    ;; compile address into bytecode: :label
	    (,(concat  "&" word)  .  font-lock-builtin-face)
	    
	    ;; put address on stack: lit :label
	    (,(concat "`" word)  .  font-lock-constant-face)
	    
	    ;; ---------------------------------------------------------
	    
	    ;; warning
	    (,(concat head (regexp-opt
			    '("tron" "troff"
			      "xx" "aregs" "fregs" "gregs" "qdump" "ddump" "bdump"
			      ) t) tail)  .  font-lock-warning-face)
	    
	    ;; jump condition
	    (,(concat head (regexp-opt
			    '(
			      "go"
			      ;;;---------------------------------------------
			      ) t) tail)  .  font-lock-builtin-face)
	    
	    ;; fvm instruction set
	    (,(concat head (regexp-opt
			    '(
			      "==="
			      ) t) tail)  .  font-lock-type-face)
	    
	    ;; mynasm keywords
	    (,(concat head (regexp-opt '("BEGIN" "END" "slot" "type" "instr"  "symbols"
					 ".while" ".loop" ".done"
					 ".while_2"  ".loop_2" ".done_2"
					 "%%while" "%%loop" "%%done"
					 "%%while_2" "%%loop_2" "%%done_2"
					 "%%all_done"
					 "%include"
					 "%undef"
					  "TX") t) tail)		.  font-lock-keyword-face)
	    ;; ---------------------------------------------------------
	    ;; assembler instructions
	    (,(concat head (regexp-opt '(
		"mov" "movl" "movb"
		"add" "addl" "addq"
		"sub"
		"mul" "imul"
		"div" "cdq" "cqo"
		"xor" "xorl" "and" "or"
		"bt" "btr" "btc" "bts"
		"inc" "dec" "neg" "shl" "sar" "bswap" "not"
		"cmp" "test"
		"call" "jmp" 
		"ret" "push" "pop"
		"syscall" "int"
		"callsave" "syscall_4" "error" "headline" "callfun"
		"gensym_next" "gensym"
		"up" "down"
		"List.next"
		"exec"

		"cprint" "print0" "iprint" "iprint_" "lf" "spc" "strprint"
		"info" "clr" "rdrop"
		"msleep" "sleep"
		"int" "sym" "lst" "arr" "uti" "untyp" "unlst" "unsym" "unarr"
		"int?" "sym?" "lst?" "arr?" "nil?" "notnil?" "notint?" "notsym?" "notlst?" "notarr?"
		"zero?" "notzero?"
		"wall?"
			      ) t) tail)  .  font-lock-type-face)

	    ;; assembler directives
	    (,(concat head (regexp-opt '(
		"KiB"
		".global" ".text" ".bss" ".data"
		"dq" "dd" "db" "resb" "resd" "resq"
		"section"
		"global" "main"
		"%endmacro" "endstruc" "iend" "istruc"
		"jz" "jnz" "js" "jns" "jge" "jle" "je" "jl" "jg" "jc" "jnc" "jne" "loop"
			      ) t) tail)  .  font-lock-builtin-face)
	    
	    ;; ---------------------------------------------------------
	    ;; numbers
	    (,(concat head (regexp-opt
			    '("some" "nil" "fail" "true" "false" "ok" "one" "zero"
			      "scp_1" "scp_2" "scp_3" "scp_4" "scp_5" "scp_6" "scp_7" 
			      ) t) tail)  .  font-lock-constant-face)
	    
	    (,(concat head "-[0-9\\.]+" tail)    .  font-lock-builtin-face)
	    (,(concat head "[\\+]?[0-9]+" tail)  .  font-lock-constant-face)
	    ;; ---------------------------------------------------------
	    
	    )))
    faces))

;;=============================================================================

(defvar mynasm-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "<return>") 'align-newline-and-indent)
    (define-key map (kbd "<tab>")    'tab-to-tab-stop)
    map))

(define-derived-mode   mynasm-mode prog-mode "mynasm" 
  "Define major mode for editing mynasm files."
  (setf font-lock-defaults       (list (mynasm-font-lock-function)))
  )

(add-to-list 'auto-mode-alist '("\\.mynasm\\'" . mynasm-mode))

(add-hook 'mynasm-mode-hook
	  (lambda ()
	    (define-key mynasm-mode-map (kbd "<return>")   'align-newline-and-indent)
	    ))

;; :section:   word characters
(mapcar (lambda (p)
	  (modify-syntax-entry (car p) (cadr p) mynasm-mode-syntax-table))
	'((?:   "W")     ; take :_?...!@ as word chars
	  (?_   "W")
	  (??   "W")
	  (?+   "W")
	  (?-   "W")
	  (?*   "W")
	  (?/   "W")
	  (?<   "W")
	  (?>   "W")
	  (?=   "W")
	  (?.   "W")
	  (?[   "W")
	    (?]   "W")
	  (?#   "W")
	  (?$   "W")
	  (?!   "W")
	  (?@   "W")
	  (?~   "W")
	  (?\&   "W")
	  (?\\   "W")
	  (?`   "W")
	  
	  (?'    "\"")    ; "'" is string delimiter: highlight 'x' like string "x"
	  (?`    "\"")    ; "'" is string delimiter: highlight 'x' like string "x"

  	  (?;  "< b")      ; ; starts comment
	   (?\n "> b")      ; ; comment up to end of line

	   
	   ))

(provide 'mynasm-mode)

;;=============================================================================

