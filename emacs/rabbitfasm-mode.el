;;=============================================================================
;;
;; :file:   rabbitfasm-mode.el		emacs syntax highlighting for rabbits FASM sources
;;
;;=============================================================================
;;
;; README
;;
;;
;;
;; CONTACT
;;
;; https:/bitbucket.org/sts-q/rabbit-vm
;;
;;
;; LICENCE
;;
;; GPL 3.0
;;
;;
;;     -*- mode: fasm -*-
;;
;;=============================================================================
;; :section:   fasm-font-lock-function
;;-----------------------------------------------------------------------------

(defun fasm-font-lock-function()
  "return a ( regexp . color ) list"
  (let* ((head   "\\<")                
	 (tail   "\\>")
	 (word   "[[:word:]]+")
	 (stack-comment   "( [^)]* )")
	 (faces 
	  `( 
	    ;; ---------------------------------------------------------
	    (,(concat "/\\*" ".*" "\\*/") . font-lock-comment-face)
	    ;; ---------------------------------------------------------
	    ;; one line macro
	     (,(concat "\\(" head "%define" tail "\\)\\s-*\\(" word "\\)")         ; {[^}]*}
	      (1  font-lock-keyword-face)
	      (2  font-lock-function-name-face))

	     (,(concat "\\(" head "def" tail "\\)\\s-*\\(" word "\\)")         ; {[^}]*}
	      (1  font-lock-keyword-face)
	      (2  font-lock-function-name-face))

	     (,(concat "\\(" head "%assign" tail "\\)\\s-*\\(" word "\\)")         ; {[^}]*}
	      (1  font-lock-keyword-face)
	      (2  font-lock-function-name-face))

	    ;; multi line macro
	     (,(concat "\\(" head "macro" tail "\\)\\s-*\\(" word "\\)")         ; {[^}]*}
	      (1  font-lock-keyword-face)
	      (2  font-lock-function-name-face))

	    ;; multi line macro
	     (,(concat "\\(" head "struc" tail "\\)\\s-*\\(" word "\\)")         ; {[^}]*}
	      (1  font-lock-keyword-face)
	      (2  font-lock-function-name-face))

	    ;; label
	    (,(concat word "\\:")  .  font-lock-variable-name-face)
	    
	    
	    ;; compile address into bytecode: :label
	    (,(concat  "&" word)  .  font-lock-builtin-face)
	    
	    ;; put address on stack: lit :label
	    (,(concat "`" word)  .  font-lock-constant-face)
	    
	    ;; ---------------------------------------------------------
	    
	    ;; warning
	    (,(concat head (regexp-opt
			    '("tron" "troff"
			      "xx" "aregs" "fregs" "gregs" "qdump" "ddump" "bdump"
			      ) t) tail)  .  font-lock-warning-face)
	    
	    ;; jump condition
	    (,(concat head (regexp-opt
			    '(
			      "go"
			      ;;;---------------------------------------------
			      ) t) tail)  .  font-lock-builtin-face)
	    
	    ;; fvm instruction set
	    (,(concat head (regexp-opt
			    '(
			      "==="
			      ) t) tail)  .  font-lock-type-face)
	    
	    ;; fasm keywords
	    (,(concat head (regexp-opt '("BEGIN" "END" "slot" "type" "instr"  "symbols"
					 ".while" ".loop" ".done"
					 ".while_2"  ".loop_2" ".done_2"
					 "%%while" "%%loop" "%%done"
					 "%%while_2" "%%loop_2" "%%done_2"
					 "%%all_done"
					 "include" "format" "segment" "entry"
					 "%undef"
					 "doloop"
					  "TX") t) tail)		.  font-lock-keyword-face)
	    ;; ---------------------------------------------------------
	    ;; assembler instructions
	    (,(concat head (regexp-opt '(
		"mov" "movl" "movb"
		"cmovl" "cmove" "cmovg" "cmovb" "cmovne" "cmovz" "cmovnz" "cmovnl"
		"1drop" "dspDrop" "rspDrop"
		"add" "addl" "addq"
		"sub" "xchg"
		"mul" "imul" "idiv"
		"div" "cdq" "cqo"
		"xor" "xorl" "and" "or"
		"bt" "btr" "btc" "bts"
		"inc" "dec" "neg" "shl" "shr" "sar" "bswap" "not"
		"cmp" "test"
		"call" "jmp" 
		"ret" "push" "pop"
		"syscall" "int"
		"callsave" "syscall_4" "error" "headline" "callfun"
		"gensym_next" "gensym"
		"up" "down" "shiftup" "shiftdown"
		"List.next"
		"exec" "exec_v"
		"cons" "uncons" "unconsLst" "consLst" "concLst"
                "check_rs_overflow"

		"cprint" "print0" "iprint" "iprint_" "lf" "spc" "strprint" "strprint2" "strprint0" "mess" "xprint"
		"clist_to_buffer"
		"info" "clr" "rdrop" "mov_a"
		"msleep" "sleep"
		"int" "sym" "lst" "arr" "uti" "flt" "unint" "untyp" "unlst" "unsym" "unarr" "unflt"
                "tagint" "untagint"
		"int?" "sym?" "lst?" "arr?" "nil?" "flt?"
		"notnil?" "notint?" "notsym?" "notlst?" "notarr?"
		"noint?" "nosym?" "nolst?" "noflt?"
		"err_if_nolst" "err_if_nosym" "err_if_noint" "err_if_nil" "err_if_noflt"
		"wall?" "List.link"
		"Stacks.print" "Stacks.clear"
			      ) t) tail)  .  font-lock-type-face)

	    ;; assembler directives
	    (,(concat head (regexp-opt '(
		"KiB"
		".global" ".text" ".bss" ".data"
		"dq" "dd" "db" "resb" "resd" "resq"
		"section"
		"global" "main"
		"%endmacro" "endstruc" "iend" "istruc"
		"jz" "jnz" "js" "jns" "jge" "jle" "je" "jl" "jg" "jc" "jnc" "jne" "loop"
		"until_zero" "until_equal" "until_notzero"
		"zero?" "notzero?" "equal?" "notequal?" "error?" "null?" "notnull?" "true?" "false?"
		"@f" "@b"
			      ) t) tail)  .  font-lock-builtin-face)
	    
	    ;; ---------------------------------------------------------
	    ;; numbers
	    (,(concat head (regexp-opt
			    '(
                              "zero"
                              "nil"
                              "one"  "true"
                              "null" "false"
                              "minus_one"
			      ) t) tail)  .  font-lock-constant-face)
	    
	    (,(concat head "-[0-9\\.]+" tail)    .  font-lock-builtin-face)
	    (,(concat head "[\\+]?[0-9]+" tail)  .  font-lock-constant-face)
	    ;; ---------------------------------------------------------
	    
	    )))
    faces))

;;=============================================================================

(defvar fasm-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "<return>") 'align-newline-and-indent)
    (define-key map (kbd "<tab>")    'tab-to-tab-stop)
    map))

(define-derived-mode   fasm-mode prog-mode "fasm" 
  "Define major mode for editing fasm files."
  (setf font-lock-defaults       (list (fasm-font-lock-function)))
  )

(add-to-list 'auto-mode-alist '("\\.fasm\\'" . fasm-mode))

(add-hook 'fasm-mode-hook
	  (lambda ()
	    (define-key fasm-mode-map (kbd "<return>")   'align-newline-and-indent)
	    ))

;; :section:   word characters
(mapcar (lambda (p)
	  (modify-syntax-entry (car p) (cadr p) fasm-mode-syntax-table))
	'((?:   "W")     ; take :_?...!@ as word chars
	  (?_   "W")
	  (??   "W")
	  (?+   "W")
	  (?-   "W")
	  (?*   "W")
	  (?/   "W")
	  (?<   "W")
	  (?>   "W")
	  (?=   "W")
	  (?.   "W")
	  (?[   "W")
	    (?]   "W")
	  (?#   "W")
	  (?$   "W")
	  (?!   "W")
	  (?@   "W")
	  (?~   "W")
	  (?\&   "W")
	  (?\\   "W")
	  (?`   "W")
	  
	  (?'    "\"")    ; "'" is string delimiter: highlight 'x' like string "x"
	  (?`    "\"")    ; "'" is string delimiter: highlight 'x' like string "x"

  	  (?;  "< b")      ; ; starts comment
	   (?\n "> b")      ; ; comment up to end of line

	   
	   ))

(provide 'fasm-mode)

;;=============================================================================

