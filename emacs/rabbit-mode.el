;;=============================================================================
;;
;; :file:   rabbit-mode.el		emacs syntax highlighting for rabbit
;;
;;=============================================================================
;;
;; README
;;
;; Rabbit is a programming language running on rabbit-vm.
;;
;; This file defines rabbit-mode for emacs. It does:
;;
;;  * syntax highlighting.
;;  * load files with file-extension '.rabbit' as rabbit-files.
;;
;;
;; INSTALL
;;
;; Put this file in your emacs load path and the following line
;; into your .emacs file.
;;
;;      (require 'rabbit-mode)
;;
;;
;; CONTACT
;;
;; https:/bitbucket.org/sts-q
;;
;;
;; LICENCE
;;
;; GPL 3.0
;;
;;
;;=============================================================================
;; :section:   rabbit-font-lock-function
;;-----------------------------------------------------------------------------

(defun rabbit-font-lock-fun()
  "return a list"
  (let* ((head   "\\<")     ; \\([[a-zA-Z0-9-_?]+\\s-+]*\\)     
	 (tail   "\\>")
	 (word		  "[[:word:]]+")
	 (name		  "[[:word:]/.-]+")
	 (stack-comment   "( [^)]* )")          ; '( '   than anything but ' )'
	 (docstring       "{[^}]*}")            ; '{'    than anything but '}'
	 (to-end-of-line  ".*$")
	 
	 ( faces 
	   `( 

	     ;; --------------------------------------------------------------
	     ;; currently not used experimental markdown sections 
	     (,(concat "^====+" )              . font-lock_keyword-face)
	     (,(concat "^----+" )              . font-lock-keyword-face)
	     (,(concat "^### " to-end-of-line) . font-lock-keyword-face)
	     (,(concat "^## "  to-end-of-line) . font-lock-keyword-face)
	     (,(concat "^# "   to-end-of-line) . font-lock-keyword-face)
	     
	     ;; --------------------------------------------------------------
	     ;; module my-module-name
	     (,(concat "\\(" head "module" tail "\\)\\s-*\\(" name "\\)")       
	      (1  font-lock-keyword-face)
	      (2  font-lock-keyword-face)
	      )
	     
	     ;; currently not used: end-of my-module-name
	     (,(concat "\\(" head "end-of" tail "\\)\\s-*\\(" name "\\)")    
	      (1  font-lock-comment-face)
	      (2  font-lock-keyword-face)
	      )
	     
	     ;; --------------------------------------------------------------
	     ;; my-definition {docstring}
	     (,(concat "\\(" name "\\)\\s-*\\(" docstring "\\)")   
	      (1  font-lock-function-name-face)
	      (2  font-lock-doc-face))
	     
             ;; label:
	     (,(concat head name "\\:" tail)		       .  font-lock-variable-name-face)
	     
             ;; {docstring}
	     (,(concat  "{[^}]*}" )                                     .  'font-lock-doc-face)

	     ;; module and define related keywords
	     (,(concat head (regexp-opt '("end" "BEGIN" "END" "defs" "xdefs" "Letf.define" "Parts.define"
					  "let" "in" "loc" "with" "where" "funcs" "functions"
					   "interface" "implementation" 
					  "loop" "loop^" "loop^^"  "recur" "rec"
					  "testing" "live" "J" "I"
					  "documentation" "inroot" "end-of-module" "mend" "modend" "module-end"
					  "root" "Define" "Define.defs"
					  "TX") t) tail)		.  font-lock-keyword-face)
	     
             ;; debugging relates keywords
	     (,(concat head (regexp-opt '("halt" "look" "xx"
					  "?" "?/" "??" "?/?" "??/" "?/?/" "??//" "???" "????"
					  ) t) tail)                    .  font-lock-warning-face)
	     
             ;; word-comment or outcommented single words: do this ,ignore-me and now that
	     (,(concat head "[\\,]\\s-*" word  tail )			. font-lock-comment-face)

;;             use , to out-comment a single word:  a b c ,skip-me d e f
;;             ;; de-emphasize (, |, )	     
;;	     (,(concat  "[()|]" )                                       .  font-lock-comment-face)
	     
             ;; keywords
	     (,(concat head (regexp-opt '("->" "->*" "->!*" "set" "def"
					  "define" "begin" ;"end"
					  "if"     "if*"     "if/" "ifnot" "ifnot*" "ifnot/"
					  "unless" "unless*" "unless/"
					  "when"   "when*"   "when/"
					  "?return"
					  "for-i" "for-i+" "for-i++"
					  "cond"   "cond/"   "cond*"   "case" "else" "others" 
					  "while"  "until"
					  "linrec" "tailrec" "binrec" "condrec" "unirec" "unirecdo"
					  "nil?*" "nild?*" "not"
					  ">" ">=" "!=" "==" "<=" "<"
					  "==*"
					  "0=="
					  "cmp" "within?" "within?*"
					  ) t) tail) .  font-lock-builtin-face)

             ;; special symbols
	     (,(concat head (regexp-opt '("true" "false"
					  "nothing" "success" "failure" "failed"
					  "pad" "mark" ) t) tail)       .  font-lock-constant-face)
             ;; emphasize #
	     (,(concat "[#]" )					        .  font-lock-builtin-face)
	     
             ;; negative numbers
	     (,(concat  "[[:space:]]\\-[0-9\\.]+" tail)                 .  font-lock-builtin-face)

             ;; numbers
	     (,(concat  "[[:space:]][\\+]?[0-9\\.]+" tail)              .  font-lock-constant-face)

	     ))
	 )
    faces))

;;=============================================================================
;; :section:   rabbit-mode
;;-----------------------------------------------------------------------------


(defvar rabbit-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "<return>") 'align-newline-and-indent)
    (define-key map (kbd "<tab>")    'tab-to-tab-stop)
    map))

(define-derived-mode rabbit-mode prog-mode "rabbit" 
  "Major mode for editing rabbit files."
  (setf font-lock-defaults       (list (rabbit-font-lock-fun)))
  )

(add-to-list 'auto-mode-alist '("\\.rabbit\\'" . rabbit-mode))


;; :section:   word characters
(mapcar (lambda (p)
	  (modify-syntax-entry (car p) (cadr p) rabbit-mode-syntax-table))
	'((?:  "w"  )      ; : is word char --> my:word is a word
	  (?_  "w"  )      ; _ is word char --> my_word is a word
          ; - / . not word-char others sexpr and word are more or less the same
          ; _ is word-char:  __test Xterm.__demo   etc (different to lisp-behavior)
;;	  (?-  "w"  )      
;;	  (?/  "w"  )
;;	  (?.  "w"  )
	  (?+  "w"  )
	  (??  "w"  )
	  (?*  "w"  )
	  (?^  "w"  )
	  (?`  "w"  )
	  (?>  "w"  )
	  (?<  "w"  )
	  (?=  "w"  )
	  (?,  "w"  )
	  (?~  "w"  )
	  (?!  "w"  )
	  (?[  "w"  )
	  (?]  "w"  )
	  (?'  "\"" )      ; ' ist string-escape char
	  (?;  "< b")      ; ; starts comment
	   (?\n "> b")      ; ; comment up to end of line
	   ))


(provide 'rabbit-mode)
;;=============================================================================
